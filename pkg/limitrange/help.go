package limitrange

import (
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

// DisplayHelpCmdLimitRangeCreate display the help message for the
// sxlimits create sub-command
func DisplayHelpCmdLimitRangeCreate() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the create limitrange help message")
	helpMessage := `
create sub-command allow you to create a new limitRange
limitRange based on the given template. If limitrange already exist, 
it will return an error 

Usage: sxlimits create NAME [TEMPLATE] [OPTIONS]...

Arguments:
  NAME       The name of the limitrange (mandatory)
  TEMPLATE   The name of the template (optional, use default template if not provided)

Generic options:
  --debug    Activates debug mode for detailed troubleshooting information.
  --help     Displays this help message and exits.

Examples:
  Create the example-limit limitRange based on the default template:
  $ sxlimits create example-limit
  Create the minmax-limit limitRange based on the minmax template:
  $ sxlimits create minmax-limit minmax

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdLimitRangeUpdate display the help message for the
// sxlimits update sub-command
func DisplayHelpCmdLimitRangeUpdate() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the update limitrange help message")
	helpMessage := `
  update sub-command allow you to update a new limitRange
limitRange based on the given template. If limitrange already exist, 
it will return an error 

Usage: sxlimits update [NAME] [TEMPLATE] [OPTIONS]...

Arguments:
  NAME                   The name of the limitRange to update. (optional, use SXLIMITS_NAME env var or 'default' if not provided)
  TEMPLATE               The name of the template (optional, use default template if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXLIMITS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXLIMITS_NAME          Define the name of the limitRange to use if not set. 
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
Update the example-limit limitRange based on the default template:
  $ sxlimits update example-limit
  Update the minmax-limit limitRange based on the minmax template:
  $ sxlimits update minmax-limit minmax

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdLimitRangeCreate display the help message for the
// sxlimits create sub-command
func DisplayHelpCmdLimitRangeGenerate() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the generate limitrange help message")
	helpMessage := `
generate sub-command allow you to generate a new limitRange
limitRange based on the given template. If template is found, it
will display the limitRange named and without namespace definition.
Ready to be piped into a kubectl apply command.

Usage: sxlimits generate [NAME] [TEMPLATE] [OPTIONS]...

Arguments:
  NAME                   The name of the limitRange to generate. (optional, use SXLIMITS_NAME env var or default if not provided)
  TEMPLATE               The name of the template (optional, use default template if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXLIMITS_NS env var or current namespace if not provided)

Environment variable:
  SXLIMITS_NAME          Define the name of the limitRange to use if not set. 
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Display the example-limit limitRange based on the default template:
  $ sxlimits generate example-limit
  Display the minmax-limit limitRange based on the minmax template:
  $ sxlimits generate minmax-limit minmax
  Generate and manualy apply the k8s-limit based on the k8s template:
  $ sxlimits generate k8s-limit k8s | kubectl apply -f -

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdLimitRangeCreate display the help message for the
// sxlimits create sub-command
func DisplayHelpCmdLimitRangeApply() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the apply limitrange help message")
	helpMessage := `
apply sub-command allow you to apply a template into an existing
limitRange. If limitrange doesn't exist, it will return an error 

Usage: sxlimits apply [NAME] [TEMPLATE] [OPTIONS]...

Arguments:
  NAME                   The name of the limitRange to update. (optional, use SXLIMITS_NAME env var or default if not provided)
  TEMPLATE               The name of the template (optional, use default template if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXLIMITS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXLIMITS_NAME          Define the name of the limitRange to use if not set. 
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Create the example-limit limitRange based on the stack-minimal template
  and add network and storage limit to it :
  $ sxlimits create example-limit stack-minimal
  $ // display the content of example-limit
  $ kubectl describe limitrange example-limit
  $ // add the storage content to it
  $ sxlimits apply example-limit stack-storage
  $ // display the content of example-limit
  $ kubectl describe limitrange example-limit
  $ // add the network content to it
  $ sxlimits apply example-limit stack-network
  $ // display the content of example-limit
  $ kubectl describe limitrange example-limit

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdLimitRangeResize display the help message for the
// sxlimits resize sub-command
func DisplayHelpCmdLimitRangeResize() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the resize limitrange help message")
	helpMessage := `
resize sub-command allow you to resize an existing limitRange
limitRange based on the given template

Usage: sxlimits resize INCREMENT [NAME] [OPTIONS]...

Arguments:
  INCREMENT              The value of the increment (optional)
                         it could be a positive or negative integer or percentage
                         if not provided, resize will increment by 1
  NAME                   The name of the limitRange to update. (optional, use SXLIMITS_NAME env var or default if not provided)

Specific options:
  --scope NAME           The scope for the resizing. (Use all if not provided), Could be 
                         - all :       Resize all field found in the targeted limitRange
                         - pod :       Resize only Pod found in the targeted limitRange
                         - container : Resize only Container found in the targeted limitRange
                         - minmax:     Resize only Min and Max found in the targeted limitRange (both Pod and Container)
                         - default:    Resize only Default and DefaultRequest found in the targeted limitRange (both Pod and Container)
                         - ratio:      Resize only MaxLimitRequestRatio found in the targeted limitRange (both Pod and Container)
                         - min:        Resize only Min and DefaultRequest found in the targeted limitRange (both Pod and Container)
                         - max:        Resize only Max and Default found in the targeted limitRange (both Pod and Container)
  --namespace NAME       Use the NAME namespace. If not provided, use SXLIMITS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXLIMITS_NAME          Define the name of the limitRange to use if not set. 
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Resize the example-limit limitRange of 20% :
  $ sxlimits resize example-limit 20%
  Resize the example-limit limitRange of 3 :
  $ sxlimits resize minmax-limit 3

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdLimitRangeAdjust display the help message for the
// sxlimits adjust sub-command
func DisplayHelpCmdLimitRangeAdjust() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the adjust limitrange help message")
	helpMessage := `
adjust sub-command allow you to adjust an existing limitRange
limitRange based on the status.used informations

Usage: sxlimits adjust INCREMENT [NAME] [OPTIONS]...

Arguments:
  INCREMENT              The value of the increment (optional)
                         it could be a positive or negative integer or percentage
                         if not provided, use 0 and adjusting will be done based only 
                         on the status.used informations of the limitRange
  NAME                   The name of the limitRange to update. (optional, use SXLIMITS_NAME env var or default if not provided)

Specific options:
  --scope NAME           The scope for the adjustment. (Use all if not provided), Could be 
                         - all :       Adjust all field found in the targeted limitRange
                         - pod :       Adjust only Pod found in the targeted limitRange
                         - container : Adjust only Container found in the targeted limitRange
                         - minmax:     Adjust only Min and Max found in the targeted limitRange (both Pod and Container)
                         - default:    Adjust only Default and DefaultRequest found in the targeted limitRange (both Pod and Container)
                         - ratio:      Adjust only MaxLimitRequestRatio found in the targeted limitRange (both Pod and Container)
                         - min:        Adjust only Min and DefaultRequest found in the targeted limitRange (both Pod and Container)
                         - max:        Adjust only Max and Default found in the targeted limitRange (both Pod and Container)
  --namespace NAME       Use the NAME namespace. If not provided, use SXLIMITS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXLIMITS_NAME          Define the name of the limitRange to use if not set. 
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Adjust the example-limit limitRange of 20% :
  $ sxlimits adjust example-limit 20%
  Adjust the example-limit limitRange of 3 :
  $ sxlimits adjust example-limit 3

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdExport display the help message for the
// sxlimits export sub-command
func DisplayHelpCmdExport() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the export limitrange help message")
	helpMessage := `
export sub-command allow you to export all limitRange present in
the current namespace into a csv file. 

Usage: sxlimits export [NS_PATTERN] [OPTIONS]...

Arguments:
  NS_PATTERN              A regex to used for multiple namespace selection. (optional, use SXLIMITS_NS env var or current namespace if not provided)

Specific options:
  --no-header            Return no header (default return headers)
  --sep SEPARATOR        Use this separator instead of the default ';'
  --output FILE          Output content into the given file instead of stdout
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Export limitrange from the current namespace :
  $ sxlimits export
  Export all limitrange defined in namespaces starting by 'myproject-' :
  $ sxlimits export myproject-*
  Export all limitrange with separator '-' into /tmp/sxlimits.csv :
  $ sxlimits export '*' --sep - --output /tmp/sxlimits.csv

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdDescribe display the help message for the
// sxlimits describe sub-command
func DisplayHelpCmdDescribe() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the describe limitrange help message")
	helpMessage := `
describe sub-command allow you to display all limitRange present in
the current namespace into a terminal based command. 

Usage: sxlimits describe [NS_PATTERN] [OPTIONS]...

Arguments:
  NS_PATTERN              A regex to used for multiple namespace selection. (optional, use SXLIMITS_NS env var or current namespace if not provided)

Specific options:
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Describe limitrange from the current namespace :
  $ sxlimits describe
  Describe all limitrange defined in namespaces starting by 'myproject-' :
  $ sxlimits describe myproject-*

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdDescribe display the help message for the
// sxlimits describe sub-command
func DisplayHelpCmd() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the sxlimits help message")
	helpMessage := `
sxlimits allow you to interact and observe limitrange resource into your
running kubernetes cluster. 

Usage: sxlimits SUBCOMMAND [OPTIONS]...

Sub-commands:
- templates              Get the list of available templates
- template               Get information about a template
- generate               Generate a new limitrange based on a template
- create                 Create a new limitrange based on the given template
- apply                  Apply a template with the given limitrange
- update                 Merge a template with the given limitrange
- resize                 Resize limitrange definitions
- adjust                 Adjust limitrange according to running workload
- describe               Display the limitranges
- export                 Export limitrange into csv content
- version                Get the version of this package

Environment variable:
  SXLIMITS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}
