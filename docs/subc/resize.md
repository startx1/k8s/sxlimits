# sxlimits resize

resize sub-command allow you to resize an existing limitRange with an
increment that could be expressed in percentage or with a positive or
negative integer.

## Usage

```bash
sxlimits resize NAME [INCREMENT] [OPTIONS]...
```

## Arguments

| **NAME**    | **Mandatory** | **Description**                                                                                                                                                                                     |
| ----------- | ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `NAME`      | **Yes**       | The name of the limitrange                                                                                                                                                                          |
| `INCREMENT` | No            | The value of the increment. It could be a positive or negative integer or percentage. If not provided, use 0 and resizing will be done based only on the status.used informations of the limitRange |

## Specific options

| **Flag**                | **Description**                                                                                                                              |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| `--scope SCOPE`         | Define the SCOPE for this ajustment. See scope section for the list of available scope (default is all)                                      |
| `--namespace NAME`      | Use the NAME namespace. If not provided, use SXLIMITS_NS env var or current namespace if not provided                                        |
| `--kubeconfig FILEPATH` | Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided |

### Available scopes

| **Scope name** | **Description**                                                                                  |
| -------------- | ------------------------------------------------------------------------------------------------ |
| `all`          | Resize all field found in the targeted limitRange                                                |
| `pod`          | Resize only Pod found in the targeted limitRange                                                 |
| `container`    | Resize only Container found in the targeted limitRange                                           |
| `minmax`       | Resize only Min and Max found in the targeted limitRange (both Pod and Container)                |
| `default`      | Resize only Default and DefaultRequest found in the targeted limitRange (both Pod and Container) |
| `ratio`        | Resize only MaxLimitRequestRatio found in the targeted limitRange (both Pod and Container)       |
| `min`          | Resize only Min and DefaultRequest found in the targeted limitRange (both Pod and Container)     |
| `max`          | Resize only Max and Default found in the targeted limitRange (both Pod and Container)            |
                       - 
## Generic options

| **Flag**  | **Description**                                                |
| --------- | -------------------------------------------------------------- |
| `--debug` | Activates debug mode for detailed troubleshooting information. |
| `--help`  | Displays this help message and exits.                          |

The `--debug` and `--help` options are applicable to all commands for enhanced 
functionality or information.

## Examples

Resizing process only work with values of the given limitRange. If you whan't to take
into account the current workload, you should [use the adjust subcommand](adjust.md).

In order to reproduce theses examples, you could create the [**test-sxlimits** example project](../example-sxlimits.md).

### Augment all limits by 3

#### Scenario

In this example, the default `all` scope will be used. 
It mean that every value found in the limitRange will be updated. For both ***Pod*** and ***Container***: *Min*, 
*Max*, *Default*, *DefaultRequest* and *MaxLimitRequestRatio* are going to be incremented by `3`. 
It will add **3** to the current values of the limitRange. For Min and DefaultRequest, it will decrement it.
This example assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Container   cpu                20m   1      20m              20m            -
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
Pod         cpu                20m   2      -                -              -
```

#### Command to perform

```bash
sxlimits resize default "3"
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min    Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---    ---    ---------------  -------------  -----------------------
Container   memory             35Mi   5Gi    35Mi             51Mi           -
Container   cpu                3020m  4      3020m            3020m          -
Container   ephemeral-storage  -      503Mi  53Mi             53Mi           -
Pod         ephemeral-storage  -      5Gi    -                -              -
Pod         memory             35Mi   7Gi    -                -              -
Pod         cpu                3020m  5      -                -              -
```

### Reduce all limits by 3

#### Scenario

In this example, the default `all` scope will be used. 
It mean that every value found in the limitRange will be updated. For both ***Pod*** and ***Container***: *Min*, 
*Max*, *Default*, *DefaultRequest* and *MaxLimitRequestRatio* are going to be decremented by `3`. 
It will remove **3** to the current values of the limitRange.
This example assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min    Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---    ---    ---------------  -------------  -----------------------
Container   memory             35Mi   5Gi    35Mi             51Mi           -
Container   cpu                3020m  4      3020m            3020m          -
Container   ephemeral-storage  -      503Mi  53Mi             53Mi           -
Pod         ephemeral-storage  -      5Gi    -                -              -
Pod         memory             35Mi   7Gi    -                -              -
Pod         cpu                3020m  5      -                -              -
```

#### Command to perform

```bash
sxlimits resize default "-3"
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Container   cpu                20m   1      20m              20m            -
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
Pod         cpu                20m   2      -                -              -
```

### Resize default and defaultRequest by 120%

#### Scenario

In this example, the `default` scope will be used. 
It mean that for value found in the limitRange (both ***Pod*** and ***Container***), only *DefaultRequest* and 
*Default* are going to be incremented by `120%`. 
It will add **120%** of the current values in limitRange. For *DefaultRequest*, it will decrement it to reduce 
the default request in the same proportion as the increment.
This example assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                20m   1      20m              20m            -
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
```

#### Command to perform

```bash
sxlimits resize default 120% --scope default
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                20m   1      24m              24m            -
Container   ephemeral-storage  -     500Mi  60Mi             60Mi           -
Container   memory             32Mi  2Gi    39Mi             58Mi           -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
```

### Resize min and max by 80%

#### Scenario

In this example, the default `minmax` scope will be used. 
It mean that for value found in the limitRange (both ***Pod*** and ***Container***), only *Min* and 
*Max* are going to be incremented by `80%`. 
It will reduce by **80%** the current values in limitRange.
This example assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                20m   1      24m              24m            -
Container   ephemeral-storage  -     500Mi  60Mi             60Mi           -
Container   memory             32Mi  2Gi    39Mi             58Mi           -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
```

#### Command to perform

```bash
sxlimits resize default 80% --scope minmax
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                16m   800m   24m              24m            -
Container   ephemeral-storage  -     400Mi  60Mi             60Mi           -
Container   memory             26Mi  2Gi    39Mi             58Mi           -
Pod         cpu                16m   1600m  -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             26Mi  4Gi    -                -              -
```
