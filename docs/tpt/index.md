# sxlimits templates

- [Only default limitRange](default.md)
- [Only min and max limitRange](minmax.md)
- [All ind of limitRange](full.md)
- [Only pod limitRange](pod.md)
- [Only container limitRange](container.md)
- [Only compute limitRange](compute.md)
- [Only memory limitRange](memory.md)
- [Only storage limitRange](storage.md)