package templates

import (
	"errors"
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

// TemplateStack represents a stack of Template objects
type TemplateStack struct {
	list    []*Template
	display *sxUtils.CmdDisplay
}

// AddTemplate adds a template to the stack
func (ts *TemplateStack) AddTemplate(template *Template) {
	ts.display.Debug("Adding template " + template.name + " into the template stack")
	ts.list = append(ts.list, template)
}

// displayTemplate show the template stack
func (ts *TemplateStack) DisplayTemplates() {
	ts.display.Debug("Display the template stack")
	fmt.Printf("%-15s %-7s %s\n", "Name", "Enabled", "Description")
	for _, template := range ts.list {
		fmt.Printf("%-15s %-7v %s\n", template.name, template.enabled, template.desc)
	}
}

// displayTemplate show the template stack
func (ts *TemplateStack) DisplayTemplateInfo(name string) {
	ts.display.Debug("Display template " + name + " informations from the templates stack")
	template, err := ts.findByName(name)
	if err != nil {
		ts.display.ExitError(err.Error(), 20)
	}
	template.GetInfo()
}

// displayTemplate show the template stack
func (ts *TemplateStack) DisplayTemplate(name string) {
	ts.display.Debug("Display template " + name + " content from the templates stack")
	template, err := ts.findByName(name)
	if err != nil {
		ts.display.ExitError(err.Error(), 20)
	}
	template.GetContent()
}

// displayTemplate show the template stack
func (ts *TemplateStack) GetTemplate(name string) (*Template, error) {
	ts.display.Debug("Get " + name + " from the template stack")
	return ts.findByName(name)
}

// UpdateTemplate updates an existing template in the stack
func (ts *TemplateStack) UpdateTemplate(name string, updatedTemplate *Template) error {
	ts.display.Debug("Update " + name + " from the template stack")
	for i, template := range ts.list {
		if template.name == name {
			ts.list[i] = updatedTemplate
			return nil
		}
	}
	return errors.New("template not found")
}

// RemoveTemplate removes a template from the stack
func (ts *TemplateStack) RemoveTemplate(name string) error {
	ts.display.Debug("Remove " + name + " from the template stack")
	for i, template := range ts.list {
		if template.name == name {
			ts.list = append(ts.list[:i], ts.list[i+1:]...)
			return nil
		}
	}
	return errors.New("template not found")
}

// Find a template by its name from the stack
func (ts *TemplateStack) findByName(name string) (*Template, error) {
	for _, template := range ts.list {
		if template.name == name {
			return template, nil
		}
	}
	return nil, errors.New("template " + name + " not found")
}

// NewTemplateStack creates a new instance of TemplateStack
func NewTemplateStack() *TemplateStack {
	stack := &TemplateStack{}
	stack.display = sxUtils.NewCmdDisplay(GroupName)
	stack.display.Debug("Init the template stack")
	stack.AddTemplate(NewTemplate(
		"container",
		"Template designed for a container limitRange definition",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: container-limits
spec:
  limits:
  - type: Container
    default:
      cpu: 200m
      ephemeral-storage: 4Gi
      memory: 256Mi
    defaultRequest:
      cpu: 100m
      ephemeral-storage: 4Gi
      memory: 128Mi
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi`,
		true))
	stack.AddTemplate(NewTemplate(
		"pod",
		"Template designed for a Pod limitRange definition",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: pod-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi`,
		true))
	stack.AddTemplate(NewTemplate(
		"default",
		"Template designed for a default limitRange definition",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: default-limits
spec:
  limits:
  - type: Container
    default:
      cpu: 200m
      ephemeral-storage: 4Gi
      memory: 256Mi
    defaultRequest:
      cpu: 100m
      ephemeral-storage: 4Gi
      memory: 128Mi`,
		true))
	stack.AddTemplate(NewTemplate(
		"minmax",
		"Template designed for minimum and maximum limitRange definition",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: minmax-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
  - type: Container
    max:
      cpu: "6"
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi`,
		true))
	stack.AddTemplate(NewTemplate(
		"storage",
		"Template designed for Storage limitation",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: sto-limits
spec:
  limits:
  - type: Pod
    max:
      ephemeral-storage: 4Gi
  - type: Container
    default:
      ephemeral-storage: 1Gi
    defaultRequest:
      ephemeral-storage: 20Mi
    max:
      ephemeral-storage: 4Gi`,
		true))
	stack.AddTemplate(NewTemplate(
		"memory",
		"Template designed for MEM limitation",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: mem-limits
spec:
  limits:
  - type: Pod
    max:
      memory: 5Gi
    min:
      memory: 32Mi
  - type: Container
    default:
      memory: 256Mi
    defaultRequest:
      memory: 128Mi
    max:
      memory: 5Gi
    min:
      memory: 32Mi`,
		true))
	stack.AddTemplate(NewTemplate(
		"compute",
		"Template designed for CPU limitation",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: cpu-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
    min:
      cpu: 20m
  - type: Container
    default:
      cpu: 200m
    defaultRequest:
      cpu: 100m
    max:
      cpu: "6"
    min:
      cpu: 20m`,
		true))
	stack.AddTemplate(NewTemplate(
		"full",
		"Template designed for a full limitRange definition",
		`apiVersion: v1
kind: LimitRange
metadata:
  name: full-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
  - type: Container
    default:
      cpu: 200m
      ephemeral-storage: 4Gi
      memory: 256Mi
    defaultRequest:
      cpu: 100m
      ephemeral-storage: 4Gi
      memory: 128Mi
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi`,
		true))
	return stack
}
