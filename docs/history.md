# Release history

## version 1.0.x (champagnac)

_The objectif of this release is to stabilize the full repository content and offer a stable release of sxlimits._

## version 0.4.x (chaumeil)

_The objectif of this release is to benefit from feedback and focus on creating a small community rhythm._

## version 0.3.x (chassagne)

_The objectif of this release is to communicate about the availability of this application._

## version 0.2.x (chastang)

The objectif of this release is to provide integration with the kubectl krew plugin mechanism, as well
as rpm, deb, homebrew, snap and container version.

### History

| Release                                                        | Date       | Description                  |
| -------------------------------------------------------------- | ---------- | ---------------------------- |
| [0.2.7](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.2.7) | 2025-03-10 | Update all dependencies      |
| [0.2.1](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.2.1) | 2024-11-12 | Upgrade to go version 1.23.3 |

## version 0.1.x (chauzu)

The objectif of this release is to use a common startx library for both sxlimits and sxlimits.

### History

| Release                                                        | Date       | Description                                      |
| -------------------------------------------------------------- | ---------- | ------------------------------------------------ |
| [0.1.3](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.1.3) | 2024-11-12 | Update all packages                              |
| [0.1.1](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.1.1) | 2024-10-13 | Update all packages and upgrade to sxlib v0.1.11 |
| [0.1.0](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.1.0) | 2024-10-13 | Upgrage all package and add the container binary |


## version 0.0.x (champeaux)

This version is a POC for testing purpose.

The objectif of this release is to create the repository structure and fundamentals of the project.

### History

| Release                                                          | Date       | Description                                                                                                                 |
| ---------------------------------------------------------------- | ---------- | --------------------------------------------------------------------------------------------------------------------------- |
| [0.0.23](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.23) | 2024-10-07 | Release a static binary                                                                                                     |
| [0.0.21](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.21) | 2024-10-05 | Update all dependencies packages for security                                                                               |
| [0.0.19](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.19) | 2024-09-24 | Update all dependencies packages for security                                                                               |
| [0.0.17](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.17) | 2024-09-11 | Use sxlib v0.0.9                                                                                                            |
| [0.0.15](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.15) | 2024-09-07 | Use sxlib v0.0.9                                                                                                            |
| [0.0.11](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.11) | 2024-06-08 | Internal change of the limitRange functions                                                                                 |
| [0.0.10](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.10) | 2024-06-03 | Use the --namespace flag verywhere                                                                                          |
| [0.0.9](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.9)   | 2024-05-08 | Adjust now reduce min when factor is more than 100%. Better describe display with Units.                                    |
| [0.0.7](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.7)   | 2024-05-05 | Adding support for describe and export subcommand. Now all subcommand implemented. Must improve export with multiple format |
| [0.0.5](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.5)   | 2024-05-05 | first version for command adjust                                                                                            |
| [0.0.3](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.3)   | 2024-05-04 | stable version for command template, templates, create, update, generate and apply                                          |
| [0.0.1](https://gitlab.com/startx1/k8s/sxlimits/-/tags/v0.0.1)   | 2024-05-03 | Init the first release of the sxlimit package derivated from the sxquotas project                                           |
