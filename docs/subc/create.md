# sxlimits create

create sub-command allow you to create a new limitRange based on the given template. 
If limitrange already exist, it will return an error 

## Usage

```bash
sxlimits create NAME [TEMPLATE] [OPTIONS]...
```

## Arguments

| NAME     | Mandatory | Description                                                     |
| -------- | --------- | --------------------------------------------------------------- |
| NAME     | Yes       | The name of the limitrange                                          |
| TEMPLATE | No        | The name of the template (use default template if not provided) |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Create the example-limit limitRange based on the default template.

```bash
sxlimits create example-limit
```

### Create the minmax-limit limitRange based on the minmax template.

```bash
sxlimits create minmax-limit minmax
```
