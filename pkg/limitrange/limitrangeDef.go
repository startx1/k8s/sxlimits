/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package limitrange

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	sxIncrementor "gitlab.com/startx1/k8s/go-libs/pkg/incrementor"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	"k8s.io/apimachinery/pkg/api/resource"
)

const (
	// GroupNameDef is the group name use in this package
	GroupNameDef                 = "limitrange.def"
	ddQdfInitQdfNewLimitRangeDef = "Init LimitRangeDef object"
	ddQdfInitQdfnewLimitRangeDef = "Init a new LimitRangeDef object"
	ddQdfInitQdfLoadFromString   = "Load limitrangeDef %s : %s"
	ddQdfIncrementStart          = "Start incrementing %s by %s"
	ddQdfDecrementStart          = "Start decrementing %s by %s"
	ddDebugQdfStart              = "DEBUG the limitrangeDef"
	ddDebugQdfKey                = "DEBUG -          key : %s"
	ddDebugQdfValString          = "DEBUG -   val string : %s"
	ddDebugQdfValInt             = "DEBUG -      val int : %d"
	ddDebugQdfValQty             = "DEBUG - val quantity : %s"
	ddDebugQdfValType            = "DEBUG -         type : %s"
	ddDebugQdfValUnit            = "DEBUG -         unit : %s"
	ddQdfIncrementRV             = "%s : increment : %s => %s [%s]"
	ddQdfDecrementRV             = "%s : decrement : %s => %s [%s]"
	ddQdfIncrementDetectMem      = "%s : Detected a mem/sto limit (%s)"
	ddQdfIncrementDetectCpu      = "%s : Detected a cpu limit (%s)"
	ddQdfIncrementDetectDef      = "%s : Detected a default limit (%s)"
	ddQdfIncrementDetectNegative = "%s : move from %d to %d (not negative)"
	ddQdfIncrementDoMem          = "%s : MEM move from %s to %s [%s]"
	ddQdfIncrementDoCpu          = "%s : CPU move from %s to %s [%s]"
	ddQdfIncrementDoDef          = "%s : NUM move from %s to %s [%s]"
	errQdfLoadParse              = "error parsing %s value : %v"
)

//
// Define object variables
//

// K8sClient represents a wrapper around the Kubernetes clientset.
type LimitRangeDef struct {
	enableDebug bool
	key         string
	valString   string
	valInt      int
	valQty      resource.Quantity
	valType     string
	valUnit     string
	display     *sxUtils.CmdDisplay
}

// NewLimitRangeDef create a new LimitRangeDef ready to use the clientset
// to interact with the kubernetes cluster
func NewLimitRangeDef(key string, val string, enableDebug bool) *LimitRangeDef {
	// sxUtils.DisplayDebug(GroupNameDef,ddQdfInitQdfNewLimitRangeDef)
	qdf := newLimitRangeDef(enableDebug)
	qdf.LoadFromString(key, val)
	return qdf
}

// newLimitRangeDef create a new LimitRangeDef with default (0) value set
func newLimitRangeDef(enableDebug bool) *LimitRangeDef {
	// sxUtils.DisplayDebug(GroupNameDef,ddQdfInitQdfnewLimitRangeDef)
	qty, _ := resource.ParseQuantity("0")
	qdf := &LimitRangeDef{
		enableDebug: enableDebug,
		key:         "cpu",
		valString:   "0",
		valInt:      0,
		valQty:      qty,
		valType:     "count",
		valUnit:     "int",
		display:     sxUtils.NewCmdDisplay(GroupNameDef),
	}
	return qdf
}

// DeepCopy creates a deep copy of an LimitRangeDef
func (qdf *LimitRangeDef) DeepCopy() *LimitRangeDef {
	copiedQty := qdf.valQty.DeepCopy()
	return &LimitRangeDef{
		enableDebug: qdf.enableDebug,
		key:         qdf.key,
		valInt:      qdf.valInt,
		valType:     qdf.valType,
		valUnit:     qdf.valUnit,
		valString:   qdf.valString,
		valQty:      copiedQty,
	}
}

// NewLimitRangeDef create a new LimitRangeDef ready to use the clientset
// to interact with the kubernetes cluster
func (qdf *LimitRangeDef) LoadFromString(key string, val string) *LimitRangeDef {
	if qdf.enableDebug {
		qdf.display.Debug(fmt.Sprintf(ddQdfInitQdfLoadFromString, key, val))
	}
	qdf.key = key
	qdf.valString = val
	qdf.valType = "count"
	qdf.valUnit = ""
	factor := 1
	re := regexp.MustCompile("(-?[0-9]+)([a-zA-Z]*)$")
	matches := re.FindStringSubmatch(qdf.GetValString())
	// Check if the string contains ".cpu"
	if strings.Contains(qdf.key, "/gpu") || strings.Contains(qdf.key, "cpu") {
		qdf.SetValType("cpu")
		if len(matches) > 2 && matches[2] == "m" {
			qdf.valUnit = matches[2]
			qdf.valString = matches[1]
		} else {
			qdf.valUnit = "m"
			qdf.valString = matches[1]
			iint, errParsei := strconv.Atoi(qdf.valString)
			if errParsei != nil {
				qdf.display.Error(
					fmt.Sprintf(
						errQdfLoadParse,
						qdf.GetValString(),
						errParsei),
				)
			}
			qdf.valInt = (iint * 1000)
			qdf.valString = fmt.Sprint(qdf.valInt)
		}
	} else {
		if len(matches) > 2 {
			qdf.valUnit = matches[2]
			qdf.valString = matches[1]
			switch qdf.GetValUnit() {
			case "m":
				qdf.SetValType("cpu")
			case "Pi":
				qdf.SetValType("mem")
				factor = 1024 * 1024 * 1024 * 1024 * 1024
			case "Ti":
				qdf.SetValType("mem")
				factor = 1024 * 1024 * 1024 * 1024
			case "Gi":
				qdf.SetValType("mem")
				factor = 1024 * 1024 * 1024
			case "Mi":
				qdf.SetValType("mem")
				factor = 1024 * 1024
			case "Ki":
				qdf.SetValType("mem")
				factor = 1024
			default:
				qdf.SetValType("count")
			}
		}
	}

	iint, errParsei := strconv.Atoi(qdf.GetValString())
	if errParsei != nil {
		qdf.display.ExitError(
			fmt.Sprintf(
				errQdfLoadParse,
				qdf.GetValString(),
				errParsei),
			30)
		return nil
	}
	qdf.valInt = (iint * factor)
	qty, errParse := resource.ParseQuantity(qdf.GetValString())
	if errParse != nil {
		qdf.display.ExitError(
			fmt.Sprintf(
				errQdfLoadParse,
				qdf.GetValString(),
				errParse),
			30)
		return nil
	}
	qdf.valQty = qty
	return qdf
}

// Display the content of the LimitRangeDef
func (qdf *LimitRangeDef) Debug() *LimitRangeDef {
	qdf.display.Debug(ddDebugQdfStart)
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfKey, qdf.GetKey()))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValString, qdf.valString))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValInt, qdf.valInt))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValQty, qdf.valQty.String()))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValType, qdf.valType))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValUnit, qdf.valUnit))
	return qdf
}

// Display the content of the LimitRangeDef
func (qdf *LimitRangeDef) Decrement(increment *sxIncrementor.Incrementor) error {
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfDecrementStart,
			qdf.GetKey(),
			increment.GetValStringUnit(),
		),
	)
	oldValue := qdf.GetValString()

	// revert if percentage or value to decrement
	if increment.IsPercentage() {
		if increment.GetValInt() >= 100 {
			increment.SetValString(
				fmt.Sprintf(
					"%d",
					int(math.Ceil((float64(100)/float64(increment.GetValInt()))*float64(100))),
				) + "%",
			)
		}
	} else if increment.GetValInt() > 0 {
		increment.SetValInt(increment.GetValInt() * -1)
	}

	// start decrementing
	qdf.doIncrement(increment)
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfDecrementRV,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return nil
}

// Display the content of the LimitRangeDef
func (qdf *LimitRangeDef) Increment(increment *sxIncrementor.Incrementor) error {
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementStart,
			qdf.GetKey(),
			increment.GetValStringUnit(),
		),
	)

	// start incrementing
	oldValue := qdf.GetValString()
	qdf.doIncrement(increment)
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementRV,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return nil
}

// Display the content of the LimitRangeDef
func (qdf *LimitRangeDef) doIncrement(increment *sxIncrementor.Incrementor) error {
	switch qdf.GetKey() {
	case
		"memory",
		"ephemeral-storage":
		if qdf.enableDebug {
			qdf.display.Debug(
				fmt.Sprintf(
					ddQdfIncrementDetectMem,
					qdf.GetKey(),
					qdf.GetValStringUnit(),
				),
			)
		}
		qdf.doIncrementMemory(increment)
	case
		"cpu",
		"gpu":
		if qdf.enableDebug {
			qdf.display.Debug(
				fmt.Sprintf(
					ddQdfIncrementDetectCpu,
					qdf.GetKey(),
					qdf.GetValStringUnit(),
				),
			)
		}
		qdf.doIncrementCPU(increment)
	default:
		if qdf.enableDebug {
			qdf.display.Debug(
				fmt.Sprintf(
					ddQdfIncrementDetectDef,
					qdf.GetKey(),
					qdf.GetValStringUnit(),
				),
			)
		}
		qdf.doIncrementDefault(increment)
	}
	return nil
}

// increment the CPU using sxIncrementor.
func (qdf *LimitRangeDef) doIncrementCPU(increment *sxIncrementor.Incrementor) *LimitRangeDef {
	oldValue := qdf.GetValString()
	iint, errParsei := strconv.Atoi(qdf.valString)
	if errParsei != nil {
		qdf.display.ExitError(
			fmt.Sprintf(errQdfLoadParse, qdf.valString, errParsei),
			30,
		)
		return nil
	}
	newValueCalc := (iint + increment.GetValInt())
	if increment.IsPercentage() {
		result := float64(iint) * (float64(increment.GetValInt()) / 100)
		if result <= 0.1 {
			newValueCalc = 0
		} else {
			newValueCalc = int(math.Ceil(result))
		}
	} else {
		if qdf.GetValUnit() == "m" {
			newValueCalc = (iint + (increment.GetValInt() * 1000))
		}
	}
	newValueString := fmt.Sprintf("%d%s", newValueCalc, qdf.GetValUnit())
	qdf.LoadFromString(qdf.key, newValueString)
	newValueQty := qdf.GetValQty()
	if newValueQty.Sign() <= 0 {
		newValueQty.Set(0)
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectNegative,
				qdf.GetKey(),
				newValueQty.Value(),
				0,
			),
		)
		qdf.SetValQty(newValueQty)
	}
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementDoCpu,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return qdf
}

// increment the Memory/storage using sxIncrementor.
func (qdf *LimitRangeDef) doIncrementMemory(increment *sxIncrementor.Incrementor) *LimitRangeDef {
	oldValue := qdf.GetValString()
	iint, errParsei := strconv.Atoi(qdf.valString)
	if errParsei != nil {
		qdf.display.ExitError(
			fmt.Sprintf(errQdfLoadParse, qdf.valString, errParsei),
			30,
		)
		return nil
	}
	newValueCalc := (iint + increment.GetValInt())
	if increment.IsPercentage() {
		result := float64(iint) * (float64(increment.GetValInt()) / 100)
		if result <= 0.1 {
			newValueCalc = 0
		} else {
			newValueCalc = int(math.Ceil(result))
		}
	}
	newValueString := fmt.Sprintf("%d%s", newValueCalc, qdf.GetValUnit())
	qdf.LoadFromString(qdf.key, newValueString)
	newValueQty := qdf.GetValQty()
	if newValueQty.Sign() <= 0 {
		newValueQty.Set(0)
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectNegative,
				qdf.GetKey(),
				newValueQty.Value(),
				0,
			),
		)
		qdf.SetValQty(newValueQty)
	}
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementDoMem,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return qdf
}

// increment the Memory/storage using sxIncrementor.
func (qdf *LimitRangeDef) doIncrementDefault(increment *sxIncrementor.Incrementor) *LimitRangeDef {
	oldValue := qdf.GetValString()
	iint, errParsei := strconv.Atoi(qdf.valString)
	if errParsei != nil {
		qdf.display.ExitError(fmt.Sprintf(errQdfLoadParse, qdf.valString, errParsei), 30)
		return nil
	}
	newValueCalc := (iint + increment.GetValInt())
	if increment.IsPercentage() {
		result := float64(iint) * (float64(increment.GetValInt()) / 100)
		if result <= 0.1 {
			newValueCalc = 0
		} else {
			newValueCalc = int(math.Ceil(result))
		}
	}
	newValueString := fmt.Sprintf("%d%s", newValueCalc, qdf.GetValUnit())
	qdf.LoadFromString(qdf.key, newValueString)
	newValueQty := qdf.GetValQty()
	if newValueQty.Sign() <= 0 {
		newValueQty.Set(0)
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectNegative,
				qdf.GetKey(),
				newValueQty.Value(),
				0,
			),
		)
		qdf.SetValQty(newValueQty)
	}
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementDoDef,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return qdf
}

// SetKey sets the key valueQty.
func (qdf *LimitRangeDef) SetKey(key string) *LimitRangeDef {
	qdf.key = key
	return qdf
}

// GetKey returns the GetKey valueQty.
func (qdf *LimitRangeDef) GetKey() string {
	return qdf.key
}

// SetValString sets the valString field.
func (qdf *LimitRangeDef) SetValString(val string) *LimitRangeDef {
	qdf.LoadFromString(qdf.GetKey(), val)
	return qdf
}

// GetValString returns the valString field.
func (qdf *LimitRangeDef) GetValString() string {
	return qdf.valString
}

// SetValInt sets the valInt field.
func (qdf *LimitRangeDef) SetValInt(val int) *LimitRangeDef {
	valString := strconv.Itoa(val)
	qdf.LoadFromString(qdf.GetKey(), valString)
	return qdf
}

// GetValInt returns the valInt field.
func (qdf *LimitRangeDef) GetValInt() int {
	return qdf.valInt
}

// SetValQty sets the valQty field.
func (qdf *LimitRangeDef) SetValQty(val *resource.Quantity) *LimitRangeDef {
	qdf.LoadFromString(qdf.GetKey(), val.String())
	return qdf
}

// GetValQty returns the valQty field.
func (qdf *LimitRangeDef) GetValQty() *resource.Quantity {
	return &qdf.valQty
}

// SetValType sets the valType field.
func (qdf *LimitRangeDef) SetValType(valType string) *LimitRangeDef {
	qdf.valType = valType
	return qdf
}

// GetValType returns the valType field.
func (qdf *LimitRangeDef) GetValType() string {
	return qdf.valType
}

// SetValUnit sets the valUnit field.
func (qdf *LimitRangeDef) SetValUnit(valUnit string) *LimitRangeDef {
	qdf.valUnit = valUnit
	return qdf
}

// GetValUnit returns the valUnit field.
func (qdf *LimitRangeDef) GetValUnit() string {
	return qdf.valUnit
}

// GetValStringUnit returns the valString + valUnit field.
func (qdf *LimitRangeDef) GetValStringUnit() string {
	return fmt.Sprintf("%s%s", qdf.GetValString(), qdf.GetValUnit())
}
