# sxlimits adjust

adjust sub-command allow you to adjust an existing limitRange
based on the resources limits and requests detected in the current workloads (pods).
An increment could be expressed in percentage or with a positive or
negative integer to boost the adjusting values.


## Usage

```bash
sxlimits adjust NAME [INCREMENT] [OPTIONS]...
```

## Arguments

| **NAME**    | **Mandatory** | **Description**                                                                                                                                                                                     |
| ----------- | ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `NAME`      | **Yes**       | The name of the limitrange                                                                                                                                                                          |
| `INCREMENT` | No            | The value of the increment. It could be a positive or negative integer or percentage. If not provided, use 0 and adjusting will be done based only on the status.used informations of the limitRange |

## Specific options

| **Flag**                | **Description**                                                                                                                              |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| `--scope SCOPE`         | Define the SCOPE for this ajustment. See scope section for the list of available scope (default is all)                                      |
| `--namespace NAME`      | Use the NAME namespace. If not provided, use SXLIMITS_NS env var or current namespace if not provided                                        |
| `--kubeconfig FILEPATH` | Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided |

### Available scopes

| **Scope name** | **Description**                                                                                  |
| -------------- | ------------------------------------------------------------------------------------------------ |
| `all`          | Adjust all field found in the targeted limitRange                                                |
| `pod`          | Adjust only Pod found in the targeted limitRange                                                 |
| `container`    | Adjust only Container found in the targeted limitRange                                           |
| `minmax`       | Adjust only Min and Max found in the targeted limitRange (both Pod and Container)                |
| `default`      | Adjust only Default and DefaultRequest found in the targeted limitRange (both Pod and Container) |
| `ratio`        | Adjust only MaxLimitRequestRatio found in the targeted limitRange (both Pod and Container)       |
| `min`          | Adjust only Min and DefaultRequest found in the targeted limitRange (both Pod and Container)     |
| `max`          | Adjust only Max and Default found in the targeted limitRange (both Pod and Container)            |
                       - 
## Generic options

| **Flag**  | **Description**                                                |
| --------- | -------------------------------------------------------------- |
| `--debug` | Activates debug mode for detailed troubleshooting information. |
| `--help`  | Displays this help message and exits.                          |

The `--debug` and `--help` options are applicable to all commands for enhanced 
functionality or information.

## Examples

Keep in mind that adjusting is the process of getting a picture of all the resources 
Request and Limits declared in Pods on the given namespace and using theses definitions 
to set the given limitRange. It means that you must run it in a namespace with existing
workloads in order to get the appropriate sizing and adjustment.

In order to reproduce theses examples and get some sample workload, you could create the 
[**test-sxlimits** example project](../example-sxlimits.md).

### Adjust all limits

#### Scenario

The default `all` scope will be used. 
It mean that every value found in the limitRange will be updated. For both ***Pod*** and ***Container***: *Min*, 
*Max*, *Default*, *DefaultRequest* and *MaxLimitRequestRatio* are going to be adjusted by values 
comming from the current pod declared resources limits and requests. 
In this example, we assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                20m   1      20m              20m            -
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Pod         cpu                20m   2      -                -              -
Pod         memory             32Mi  4Gi    -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
```

#### Command to perform

```bash
sxlimits adjust default
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min    Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---    ---    ---------------  -------------  -----------------------
Container   cpu                15m   800m  15m              200m           -
Container   memory             32Mi  1Gi   32Mi             50Mi           -
Container   ephemeral-storage  -     1Gi   256Mi            256Mi          -
Pod         cpu                15m   800m  -                -              -
Pod         memory             32Mi  1Gi   -                -              -
Pod         ephemeral-storage  -     1Gi   -                -              -
```

### Adjust all limits and add 3

#### Scenario

In this example, the default `all` scope will be used. 
It mean that every value found in the limitRange will be updated. For both ***Pod*** and ***Container***: *Min*, 
*Max*, *Default*, *DefaultRequest* and *MaxLimitRequestRatio* are going to be incremented by `3`. 
For *Min* and *DefaultRequest*, it will decrement it.
This example assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min    Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---    ---    ---------------  -------------  -----------------------
Container   cpu                15m   800m  15m              200m           -
Container   memory             32Mi  1Gi   32Mi             50Mi           -
Container   ephemeral-storage  -     1Gi   256Mi            256Mi          -
Pod         cpu                15m   800m  -                -              -
Pod         memory             32Mi  1Gi   -                -              -
Pod         ephemeral-storage  -     1Gi   -                -              -
```

#### Command to perform

```bash
sxlimits adjust default 3
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Container   cpu                20m   1      20m              20m            -
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
Pod         cpu                20m   2      -                -              -
```

### Adjust default and defaultRequest by 120%

#### Scenario

In this example, the `default` scope will be used. 
It mean that for value found in the limitRange (both ***Pod*** and ***Container***), only *DefaultRequest* and 
*Default* are going to be incremented by `120%`. 
It will add **120%** to the observed resource consuption of the Pods. For *DefaultRequest*, it will decrement and reduce 
the default request in the same proportion as the increment.
This example assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                20m   1      20m              20m            -
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
```

#### Command to perform

```bash
sxlimits adjust default 120% --scope default
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                20m   1      24m              24m            -
Container   ephemeral-storage  -     500Mi  60Mi             60Mi           -
Container   memory             32Mi  2Gi    39Mi             58Mi           -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
```

### Adjust min and max by 80%

#### Scenario

In this example, the default `minmax` scope will be used. 
It mean that for value found in the limitRange (both ***Pod*** and ***Container***), only *Min* and 
*Max* are going to be incremented by `80%`. 
It will reduce by **80%** the current values in limitRange.
This example assume that your limitRange is named `default`.

#### State before

Result of the `kubectl -n test-sxlimits describe limitrange` prior to the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                20m   1      24m              24m            -
Container   ephemeral-storage  -     500Mi  60Mi             60Mi           -
Container   memory             32Mi  2Gi    39Mi             58Mi           -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
```

#### Command to perform

```bash
sxlimits adjust default 80% --scope minmax
```

#### State after

Result of the `kubectl -n test-sxlimits describe limitrange` after execution of the command 
should be like the following example :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   cpu                16m   800m   24m              24m            -
Container   ephemeral-storage  -     400Mi  60Mi             60Mi           -
Container   memory             26Mi  2Gi    39Mi             58Mi           -
Pod         cpu                16m   1600m  -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             26Mi  4Gi    -                -              -
```
