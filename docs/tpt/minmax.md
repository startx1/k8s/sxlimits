# sxlimits minmax template

Template designed for a minmax limitRange definition

## Usage

```bash
sxlimits create minmax-limits minmax
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: minmax-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
  - type: Container
    max:
      cpu: "6"
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
```
