# sxlimits full template

Template designed for a full limitRange definition

## Usage

```bash
sxlimits create full-limits full
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: full-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
  - type: Container
    default:
      cpu: 200m
      ephemeral-storage: 4Gi
      memory: 256Mi
    defaultRequest:
      cpu: 100m
      ephemeral-storage: 4Gi
      memory: 128Mi
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
```
