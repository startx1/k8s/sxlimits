package main

import (
	"os"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxUtilsLimitRange "gitlab.com/startx1/k8s/sxlimits/pkg/utils"
)

const (
	ddSeqInit            = "Start the main function"
	ddSeqStart           = "Executing %s subcommand"
	ddCreateStart        = "Start creating the %s limitRange (based on %s template)"
	ddUpdateStart        = "Start updating the %s limitRange (based on %s template)"
	ddAdjustStart        = "Start adjusting the %s limitRange with %s"
	ddResizeStart        = "Start resizing the %s limitRange with %s"
	ddOkCreate           = "LimitRange %s is added into namespace %s"
	ddOkApply            = "LimitRange %s is updated into namespace %s"
	ddOkGenerate         = "LimitRange %s is generated"
	ddOkResize           = "LimitRange %s is resized into namespace %s"
	ddOkAdjust           = "LimitRange %s is adjusted into namespace %s"
	errNoSubcmd          = "Expected 'templates', 'template', 'create', 'adjust', 'resize' or 'export' subcommands. See : sxlimits help."
	errTemplateReq       = "Template name is required for 'template' command"
	errLimitRangeNameReq = "LimitRange name is required for '%s' command"
	errCreateGetTpl      = "Error accessing the template : %v"
	errUpdateGetTpl      = "Error accessing the template : %v"
	errResizeReq         = "limitRange name is required for '%s' command"
	errResizeLoad        = "Error loading limitRange %s in namespace %s : %v"
	errResizeResize      = "Error resizing limitRange %s in namespace %s : %v"
	errResizeApply       = "Error applying limitRange %s in namespace %s : %v"
	errAdjustReq         = "LimitRange name is required for 'adjust' command"
	errAdjustLoad        = "Error loading limitRange %s in namespace %s : %v"
	errAdjustAdjust      = "Error adjusting limitRange %s in namespace %s : %v"
	errAdjustApply       = "Error applying limitRange %s in namespace %s : %v"
	errApplyLoad         = "Error loading limitRange %s in namespace %s : %v"
	errApplyMerge        = "Error resizing limitRange %s in namespace %s : %v"
	errExportDo          = "Error exporting limitRange %s in namespace %s : %v"
	errApplyApply        = "Error applying limitRange %s in namespace %s : %v"
	errCreateGenerate    = "Error generating limitRange %s : %v"
	errApply             = "Error applying limitRange %s into the namespace %s : %v"
	errCreate            = "Error creating limitRange %s into the namespace %s : %v"
	errUpdateTemplateReq = "Template name is required for 'update' command"
)

func main() {
	display := sxUtils.NewCmdDisplay("main")
	display.Debug(ddSeqInit)
	argsLimitRange := sxUtilsLimitRange.NewSXLimitsArgParser(os.Args)
	argsLimitRange.Prepare()
	argsLimitRange.Exec()
}
