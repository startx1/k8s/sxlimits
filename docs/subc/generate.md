# sxlimits generate

generate sub-command allow you to generate a new limitRange based on the given template. 
If template is found, it will display the limitRange named and without namespace definition.
Ready to be piped into a kubectl apply command.

## Usage

```bash
sxlimits generate NAME [TEMPLATE] [OPTIONS]...
```

## Arguments

| NAME     | Mandatory | Description                                                     |
| -------- | --------- | --------------------------------------------------------------- |
| NAME     | Yes       | The name of the limitrange                                          |
| TEMPLATE | No        | The name of the template (use default template if not provided) |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Display the example-limit limitRange based on the default template.

```bash
sxlimits generate example-limit
```

### Display the minmax-limit limitRange based on the minmax template.

```bash
sxlimits generate minmax-limit minmax
```

### Generate and manualy apply the k8s-limit based on the k8s template.

```bash
sxlimits generate k8s-limit k8s | kubectl apply -f -
```