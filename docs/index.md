# Startx LimitRange (sxlimits) 

[![release](https://img.shields.io/badge/release-v0.2.7-blue.svg)](https://gitlab.com/startx1/k8s/sxlimits/-/releases/v0.2.7) [![last commit](https://img.shields.io/gitlab/last-commit/startx1/k8s/sxlimits.svg)](https://gitlab.com/startx1/k8s/sxlimits) [![Doc](https://readthedocs.org/projects/sxlimits/badge)](https://sxlimits.readthedocs.io) 

The `sxlimits` command line can interact with a kubernetes cluster and allow operations 
on the LimitRanges resources like resize or adjust to update the spec.hard values.

## Getting started

- [Install the `sxlimits` binary](installation.md)
- Test it with `sxlimits version`
- Log into a kubernetes cluster `kubectl login ...`
- [using the `sxlimits` binary](subc/index.md)
  - Create a new limitRange based on the default template `sxlimits create mylimits`
  - Get your limitRange to see the changes `kubectl get limitrange mylimits -o yaml`
  - Resize the limitRange definitions `sxlimits resize mylimits 3`
  - Get your limitRange to see the changes `kubectl get limitrange mylimits -o yaml`
  - Adjust the limitRange definitions `sxlimits adjust mylimits`
  - Get your limitRange to see the changes `kubectl get limitrange mylimits -o yaml`

## Contributing

You can help us by [contributing to the project](contribute.md).


## History and releases

Read history [traceback](history.md) for more information on change and released version. 
