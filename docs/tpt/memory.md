# sxlimits memory template

Template designed for a memory limitRange definition

## Usage

```bash
sxlimits create mem-limits memory
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: mem-limits
spec:
  limits:
  - type: Pod
    max:
      memory: 5Gi
    min:
      memory: 32Mi
  - type: Container
    default:
      memory: 256Mi
    defaultRequest:
      memory: 128Mi
    max:
      memory: 5Gi
    min:
      memory: 32Mi
```
