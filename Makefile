.PHONY: all clean test

# Name of the Go source file
SRC = main.go
DOCKER_COMMAND = podman
DOCKER_REPO = docker.io/startx
BIN = sxlimits

all: build

# Build the binary
build: build_bin build_docker

build_bin:
	go get -v -d ./...
	go mod tidy
	mkdir -p bin/
	CGO_ENABLED=0 
	GOOS=linux 
	GOARCH=amd64
	go build -tags 'osusergo netgo' -ldflags "-s -w -extldflags -static" -o bin/$(BIN) $(SRC)
	file bin/$(BIN)
	ls -la bin/$(BIN)

build_docker:
	VERSION=$(./bin/$(BIN) version | cut -d'v' -f2-)
	$(DOCKER_COMMAND) build . --tag $(BIN)
	$(DOCKER_COMMAND) images | grep $(BIN)

update:
	go get -u ./...

# Release the binary
release: build test

# Publish the binary
publish: publish_docker

publish_docker:
	@VERSION=$$(./bin/$(BIN) version | cut -d'v' -f2-) ; \
	$(DOCKER_COMMAND) tag localhost/$(BIN):latest $(DOCKER_REPO)/$(BIN):latest ; \
	$(DOCKER_COMMAND) tag localhost/$(BIN):latest $(DOCKER_REPO)/$(BIN):$${VERSION} ; \
	echo "--- Publishing $(DOCKER_REPO)/$(BIN):latest" ; \
	$(DOCKER_COMMAND) push $(DOCKER_REPO)/$(BIN):latest ; \
	echo "--- Publishing $(DOCKER_REPO)/$(BIN):$${VERSION}" ; \
	$(DOCKER_COMMAND) push $(DOCKER_REPO)/$(BIN):$${VERSION} 

# Run tests
test: test_code test_bin test_docker

test_code:
	echo "Running tests code..."
	go test -v ./... -coverprofile .testCoverage.txt
	cat .testCoverage.txt

test_bin:
	echo "Running binary tests..."
	./bin/$(BIN) version

test_docker:
	echo "Running docker tests..."
	$(DOCKER_COMMAND) run $(BIN) version

# Clean generated files
clean:
	rm -f $(BIN)