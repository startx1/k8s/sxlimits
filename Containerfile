FROM alpine:latest

ENV SX_VERSION="0.2.7" \
    SX_APP="sxlimits" \
    SX_ID="startx/$SX_APP" \
    SX_NAME="Startx SXQuotas image" \
    SX_SUMMARY="Container running the sxlimits toolkit for kubernetes and openshift limitrange management" \
    SX_MAINTAINER_MAIL="dev@startx.fr" \
    SX_VERBOSE=true \
    SX_DEBUG=false \
    APP_PATH="/app" \
    LOG_PATH=/var/log/sx \
    SX_S2IDIR="/tmp" \
    SX_LIBDIR="/var/lib/sx" \
    DAEMON_STOP_TIMEOUT=3 \
    DAEMON_START_INTERVAL=10

LABEL name="$SX_ID" \
    summary="$SX_SUMMARY" \
    description="$SX_SUMMARY" \
    org.opencontainers.image.description="$SX_SUMMARY" \
    version="$SX_VERSION" \
    release="$SX_VERSION" \
    org.opencontainers.image.version="$SX_VERSION" \
    maintainer="Startx <$SX_MAINTAINER_MAIL>" \
    org.opencontainers.image.vendor="STARTX" \
    io.artifacthub.package.maintainers='[{"name":"STARTX","email":"$SX_MAINTAINER_MAIL"}]' \
    io.k8s.description="$SX_SUMMARY" \
    io.artifacthub.package.license='Apache-2.0' \
    org.opencontainers.image.created='2024-10-08T00:00:00Z' \
    io.k8s.display-name="$SX_ID" \
    io.openshift.tags="startx,sxlimits,resources,quotas,sizing" \
    io.artifacthub.package.keywords='startx,sxlimits,resources,quotas,sizing' \
    io.openshift.non-scalable="true" \
    io.openshift.min-memory="64Mi" \
    io.openshift.min-cpu="100m" \
    fr.startx.component="$SX_ID:$SX_VERSION" \
    org.opencontainers.image.documentation="https://$SX_APP.readthedocs.io/en/latest/" \
    org.opencontainers.image.source="https://gitlab.com/startx1/k8s/$SX_APP/" \
    io.artifacthub.package.readme-url="https://gitlab.com/startx1/k8s/$SX_APP/-/raw/README.md" \
    io.artifacthub.package.logo-url="https://gitlab.com/startx1/k8s/$SX_APP/-/raw/docs/img/$SX_APP.svg" \
    io.artifacthub.package.alternative-locations="quay.io/startx/$SX_APP:$SX_VERSION,docker.io/startx/$SX_APP:$SX_VERSION"


COPY bin/sxlimits /usr/local/bin/sxlimits

WORKDIR /app

RUN chown 1001:0 /usr/local/bin/sxlimits
RUN chmod ugo=x /usr/local/bin/sxlimits

USER 1001

ENTRYPOINT ["/usr/local/bin/sxlimits"]
CMD ["--help"]