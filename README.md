# sxlimits [![release](https://img.shields.io/badge/release-v0.2.7-blue.svg)](https://gitlab.com/startx1/k8s/sxlimits/-/releases/v0.2.7) [![last commit](https://img.shields.io/gitlab/last-commit/startx1/k8s/sxlimits.svg)](https://gitlab.com/startx1/k8s/sxlimits) [![Doc](https://readthedocs.org/projects/sxlimits/badge)](https://sxlimits.readthedocs.io) 

This project is focused on producing a `sxlimits` command line who can interact with a kubernetes cluster
and allow operations on the LimitRanges resources.

## Getting started

- [Install the `sxlimits` binary](https://sxlimits.readthedocs.io/en/devel/installation/) [(download)](https://gitlab.com/startx1/k8s/sxlimits/-/raw/stable/bin/sxlimits)
- Test it with `sxlimits version`
- Log into a kubernetes cluster `kubectl login ...`
- Create a new limitRange based on the default template `sxlimits create mylimits`
- Get your limitRange to see the changes `kubectl get limitRange mylimits -o yaml`
- Resize the limitRange definitions `sxlimits resize mylimits 3`
- Get your limitRange to see the changes `kubectl get limitRange mylimits -o yaml`
- Adjust the limitRange definitions `sxlimits adjust mylimits`
- Get your limitRange to see the changes `kubectl get limitRange mylimits -o yaml`
