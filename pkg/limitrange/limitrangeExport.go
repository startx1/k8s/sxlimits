/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package limitrange

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"strconv"
	"strings"

	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// GroupNameExport is the group name use in this package
	GroupNameExport                    = "limitrange.export"
	ddInitLRExporter                   = "Init Exporting %s limitRange"
	ddInitListNamespaceLR              = "Init listing limitRange in namespace %s"
	ddInitExtractNamespaceLR           = "Init extracting limitRange in namespace %s"
	errExportList                      = "Error listing LimitRanges in namespace %s : %s"
	errExportWriteCSV                  = "error writing CSV header : %v"
	errExportWriteCSVLine              = "error writing CSV header for %s : %v"
	errExportDo                        = "error exporting : %v"
	errExtractNamespaceLR              = "error extracting namespace %s : %v"
	errLRExporterGetNS                 = "error getting the list of namespaces matching %s : %v"
	errLRExporterGetNSLR               = "error getting limitRange in namespace %s : %v"
	ddLRExporterStart                  = "Get all limitRange in namespaces matching %s pattern"
	ddLRExporterStartNS                = "Get all limitRange in namespace %s"
	ddLRExporterParseLR                = "%s - %s - Parsed Limits : %d"
	ddLRExporterParseLRLine            = "%s - %s - %s - %s - %s : %s"
	ddLRExporterFullParseLRR           = "%s - %s - %s - %s - %s : Write CSV line"
	ddLRExporterResourceParseLRR       = "%s - %s - %s - %s - %s : Write CSV line"
	ddLRExporterNamespaceParseLRR      = "%s - %s - %s - %s - %s : Write CSV line"
	errLRExporterWriteCSV              = "error writing CSV header : %v"
	errLRExporterFullWriteCSVLine      = "error writing CSV line for limitRange %s - %s - %s - %s - %s : %v"
	errLRExporterResourceWriteCSVLine  = "error writing CSV line for limitRange %s - %s - %s - %s - %s : %v"
	errLRExporterNamespaceWriteCSVLine = "error writing CSV line for limitRange %s - %s - %s - %s - %s : %v"
	warnPercentageOfDivisorZero        = "warning in namespace %s - limitRange %s - resourceName %s - error divisor is zero"
	ddLRExporterGenerateCSVFull        = "Start Generating full CSV report"
	ddInitLRDescriber                  = "Init Describing %s limitRange"
	errLRDescriberGetNS                = "error getting the list of namespaces matching %s : %v"
	errLRDescriberGetNSLR              = "error getting limitRange in namespace %s : %v"
	errLRDescribeGetExport             = "error exporting for describe : %v"
)

// LRExporter return the list of all resource, in all limitRange for the namespaces matching the namespacePattern
func LRExporter(namespacePattern string, k8sclient *sxKCli.K8sClient, separatorNew string, hasHeader bool, hasBrutVal bool) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitLRExporter, namespacePattern))
	var nsList []string
	var errListNS error
	separator := ','
	if separatorNew != "" {
		x := []rune(separatorNew)
		separator = x[0]
	}
	if namespacePattern == "" {
		nsList = append(nsList, k8sclient.GetCurrentNamespace())
	} else {
		nsList, errListNS = k8sclient.ListAllMatchingNamespaces(namespacePattern)
	}
	if errListNS != nil {
		display.Error(fmt.Sprintf(errLRExporterGetNS, namespacePattern, errListNS))
		return "", errListNS
	}
	display.Debug(fmt.Sprintf(ddLRExporterStart, namespacePattern))
	var results []map[string]string
	for _, namespace := range nsList {
		display.Debug(fmt.Sprintf(ddLRExporterStartNS, namespace))
		extractLR, errExtractLR := ExtractNamespaceLR(namespace, k8sclient)
		if errExtractLR != nil {
			display.Error(fmt.Sprintf(errLRExporterGetNSLR, namespace, errExtractLR))
		}
		for _, limitrange := range extractLR {
			display.Debug(fmt.Sprintf(
				ddLRExporterParseLR,
				limitrange.Namespace,
				limitrange.Name,
				len(limitrange.Spec.Limits),
			))
			for _, limits := range limitrange.Spec.Limits {
				if limits.Min != nil {
					for limitsDestName, limitsDestQty := range limits.Min {
						category := "Min"
						display.Debug(fmt.Sprintf(
							ddLRExporterParseLRLine,
							limitrange.Namespace,
							limitrange.Name,
							string(limits.Type),
							category,
							string(limitsDestName),
							limitsDestQty.String(),
						))
						limitDetails := map[string]string{
							"Namespace":      limitrange.Namespace,
							"LimitRangeName": limitrange.Name,
							"Type":           string(limits.Type),
							"Resource":       string(limitsDestName),
							"Category":       category,
							"Value":          limitsDestQty.String(),
						}
						results = append(results, limitDetails)
					}
				}
				if limits.Max != nil {
					for limitsDestName, limitsDestQty := range limits.Max {
						category := "Max"
						display.Debug(fmt.Sprintf(
							ddLRExporterParseLRLine,
							limitrange.Namespace,
							limitrange.Name,
							string(limits.Type),
							category,
							string(limitsDestName),
							limitsDestQty.String(),
						))
						limitDetails := map[string]string{
							"Namespace":      limitrange.Namespace,
							"LimitRangeName": limitrange.Name,
							"Type":           string(limits.Type),
							"Resource":       string(limitsDestName),
							"Category":       category,
							"Value":          limitsDestQty.String(),
						}
						results = append(results, limitDetails)
					}
				}
				if limits.Default != nil {
					for limitsDestName, limitsDestQty := range limits.Default {
						category := "Default"
						display.Debug(fmt.Sprintf(
							ddLRExporterParseLRLine,
							limitrange.Namespace,
							limitrange.Name,
							string(limits.Type),
							category,
							string(limitsDestName),
							limitsDestQty.String(),
						))
						limitDetails := map[string]string{
							"Namespace":      limitrange.Namespace,
							"LimitRangeName": limitrange.Name,
							"Type":           string(limits.Type),
							"Resource":       string(limitsDestName),
							"Category":       category,
							"Value":          limitsDestQty.String(),
						}
						results = append(results, limitDetails)
					}
				}
				if limits.DefaultRequest != nil {
					for limitsDestName, limitsDestQty := range limits.DefaultRequest {
						category := "DefaultRequest"
						display.Debug(fmt.Sprintf(
							ddLRExporterParseLRLine,
							limitrange.Namespace,
							limitrange.Name,
							string(limits.Type),
							category,
							string(limitsDestName),
							limitsDestQty.String(),
						))
						limitDetails := map[string]string{
							"Namespace":      limitrange.Namespace,
							"LimitRangeName": limitrange.Name,
							"Type":           string(limits.Type),
							"Resource":       string(limitsDestName),
							"Category":       category,
							"Value":          limitsDestQty.String(),
						}
						results = append(results, limitDetails)
					}
				}
				if limits.MaxLimitRequestRatio != nil {
					for limitsDestName, limitsDestQty := range limits.MaxLimitRequestRatio {
						category := "MaxLimitRequestRatio"
						display.Debug(fmt.Sprintf(
							ddLRExporterParseLRLine,
							limitrange.Namespace,
							limitrange.Name,
							string(limits.Type),
							category,
							string(limitsDestName),
							limitsDestQty.String(),
						))
						limitDetails := map[string]string{
							"Namespace":      limitrange.Namespace,
							"LimitRangeName": limitrange.Name,
							"Type":           string(limits.Type),
							"Resource":       string(limitsDestName),
							"Category":       category,
							"Value":          limitsDestQty.String(),
						}
						results = append(results, limitDetails)
					}
				}
			}
		}
	}
	csvContent, errGen := LRExporterGenerateCSVFull(separator, hasHeader, results, hasBrutVal)
	if errGen != nil {
		return "", fmt.Errorf(errLRExporterWriteCSV, errGen)
	}

	return csvContent, nil
}

// LRExporter return the list of all resource, in all limitRange for the namespaces matching the namespacePattern
func LRDescriber(namespacePattern string, k8sclient *sxKCli.K8sClient) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitLRDescriber, namespacePattern))

	csvContent, errExport := LRExporter(namespacePattern, k8sclient, ",", true, true)
	if errExport != nil {
		display.Error(fmt.Sprintf(errLRDescribeGetExport, errExport))
		return "", errExport
	}

	// Create a new CSV reader
	reader := csv.NewReader(strings.NewReader(csvContent))

	// Read all records from the CSV
	records, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Error:", err)
		return "", err
	}

	// Find maximum length for each column
	maxColLengths := make([]int, len(records[0]))
	for _, row := range records {
		for i, column := range row {
			if len(column) > maxColLengths[i] {
				maxColLengths[i] = len(column) + 1
			}
		}
	}

	// Display each line with fixed column lengths
	output := ""
	for j, row := range records {
		for i, column := range row {
			output = output + fmt.Sprintf("%-*s ", maxColLengths[i], column)
		}
		output = output + "\n"
		// Print separation line after the first header line
		if j == 0 {
			for _, length := range maxColLengths {
				output = output + strings.Repeat("-", length) + " "
			}
			output = output + "\n"
		}
	}

	return output, nil
}

// LRExporterGenerateCSVFull return the CSV content for the given resultSet
func LRExporterGenerateCSVFull(separator rune, hasHeader bool, resultSet []map[string]string, hasBrutVal bool) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(ddLRExporterGenerateCSVFull)

	// Create a buffer and CSV writer to write into the buffer
	var csvOutput bytes.Buffer
	writer := csv.NewWriter(&csvOutput)
	writer.Comma = separator
	// Write CSV headers
	if hasHeader {
		errWH := writer.Write([]string{"Namespace", "LimitRangeName", "Type", "Resource", "Category", "Value"})
		if errWH != nil {
			return "", fmt.Errorf(errLRExporterWriteCSV, errWH)
		}
	}
	// Write CSV lines
	for _, qt := range resultSet {
		limitDef := NewLimitRangeDef(qt["Resource"], qt["Value"], false)
		limitVal := ""

		if hasBrutVal {
			limitVal = limitDef.GetValStringUnit()
		} else {
			switch limitDef.GetValUnit() {
			case "m", "Ki", "Mi", "Gi", "Ti", "Pi":
				limitVal = strconv.Itoa(limitDef.valInt)
			default:
				limitVal = limitDef.GetValString()
			}
		}
		display.Debug(fmt.Sprintf(
			ddLRExporterFullParseLRR,
			qt["Namespace"],
			qt["LimitRangeName"],
			qt["Type"],
			qt["Resource"],
			qt["Category"],
		))
		errWL := writer.Write([]string{
			qt["Namespace"],
			qt["LimitRangeName"],
			qt["Type"],
			qt["Resource"],
			qt["Category"],
			limitVal,
		})
		if errWL != nil {
			return "", fmt.Errorf(errLRExporterFullWriteCSVLine,
				qt["Namespace"],
				qt["LimitRangeName"],
				qt["Type"],
				qt["Resource"],
				qt["Category"],
				errWL,
			)
		}
	}
	// Persist CSV content
	writer.Flush()

	return csvOutput.String(), nil
}

// ListNamespaceLR return the list of all limitrange in a namespace
func ListNamespaceLR(namespace string, k8sclient *sxKCli.K8sClient) ([]string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitListNamespaceLR, namespace))
	limitrange, err := k8sclient.
		Clientset.
		CoreV1().
		LimitRanges(namespace).
		List(
			context.TODO(),
			metav1.ListOptions{},
		)
	if err != nil {
		return nil, err
	}

	var limitNames []string
	for _, lr := range limitrange.Items {
		limitNames = append(limitNames, lr.Name)
	}
	return limitNames, nil
}

// ExtractNamespaceLR grab all limitRange from a namespace and return a list of v1.LimitRange
func ExtractNamespaceLR(namespace string, k8sclient *sxKCli.K8sClient) ([]v1.LimitRange, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitExtractNamespaceLR, namespace))
	// Get all LimitRanges in the namespace
	limitrangeList, errList := k8sclient.
		Clientset.
		CoreV1().
		LimitRanges(namespace).
		List(
			context.TODO(),
			metav1.ListOptions{},
		)
	if errList != nil {
		display.Error(fmt.Sprintf(errExtractNamespaceLR, namespace, errList))
		return nil, errList
	}
	return limitrangeList.Items, nil
}

// // ExportNamespaceLR export limitRange informations into a csv format
// func ExportNamespaceLR(namespace string, k8sclient *sxKCli.K8sClient) (string, error) {
// 	display := sxUtils.NewCmdDisplay(GroupNameExport)
// 	display.Debug(ddInitExportNamespaceLR)
// 	// Get all LimitRanges in the namespace
// 	The name of the limitrangeList, errList := k8sclient.
// 		Clientset.
// 		CoreV1().
// 		LimitRanges(namespace).
// 		List(
// 			context.TODO(),
// 			metav1.ListOptions{},
// 		)
// 	if errList != nil {
// 		display.ExitError(
// 			fmt.Sprintf(
// 				errExportList,
// 				namespace,
// 				errList,
// 			),
// 			30,
// 		)
// 	}
// 	// Maps to hold sums of resources
// 	totalContainer := make(map[corev1.ResourceName]resource.Quantity)
// 	totalPod := make(map[corev1.ResourceName]resource.Quantity)
// 	// Aggregate data across all The name of the limitrange
// 	for _, limit := range The name of the limitrangeList.Items {
// 		addLRLimitRange2Total("Container", totalContainer, limit.Status.Container)
// 		addLRLimitRange2Total("Pod", totalPod, limit.Status.Pod)
// 	}
// 	// Create a buffer and CSV writer to write into the buffer
// 	var csvOutput bytes.Buffer
// 	writer := csv.NewWriter(&csvOutput)
// 	writer.Comma = ';'
// 	// Write CSV headers
// 	errWH := writer.Write([]string{"Resource", "Total Container", "Total Pod"})
// 	if errWH != nil {
// 		return "", fmt.Errorf(errExportWriteCSV, errWH)
// 	}
// 	// Process each resource type found in totalContainer or totalPod
// 	for resource := range totalContainer {
// 		hard := totalContainer[resource]
// 		used := totalPod[resource]
// 		errWL := writer.Write([]string{string(resource), hard.String(), used.String()})
// 		if errWL != nil {
// 			return "", fmt.Errorf(errExportWriteCSVLine, string(resource), errWL)
// 		}
// 	}
// 	writer.Flush()
// 	return csvOutput.String(), nil
// }

// // addLRLimitRange2Total adds resources from one ResourceList into another ResourceList map
// func addLRLimitRange2Total(kind string, total map[corev1.ResourceName]resource.Quantity, rs corev1.ResourceList) {
// 	display := sxUtils.NewCmdDisplay(GroupNameExport)
// 	for k, v := range rs {
// 		valueDisplay := ""
// 		if existing, ok := total[k]; ok {
// 			newValue := existing.DeepCopy()
// 			switch k {
// 			case
// 				"requests.memory",
// 				"limits.memory",
// 				"requests.storage",
// 				"ephemeral-storage":
// 				nextMili := existing.MilliValue() + v.MilliValue()
// 				display.Debug(fmt.Sprintf("for %s : move from %d to %d (mem)", k.String(), existing.MilliValue(), nextMili))
// 				newValue.SetMilli(nextMili)
// 			case
// 				"requests.cpu",
// 				"limits.cpu":
// 				nextMili := existing.MilliValue() + v.MilliValue()
// 				display.Debug(fmt.Sprintf("for %s : move from %d to %d (cpu)", k.String(), existing.MilliValue(), nextMili))
// 				newValue.SetMilli(nextMili)
// 			default:
// 				nextVal := existing.Value() + v.Value()
// 				display.Debug(fmt.Sprintf("for %s : move from %d to %d (cpu)", k.String(), existing.Value(), nextVal))
// 				newValue.Set(nextVal)
// 			}
// 			total[k] = newValue
// 			valueDisplay = newValue.String()
// 		} else {
// 			valueDisplay = v.String()
// 			total[k] = v.DeepCopy()
// 		}
// 		display.Debug(fmt.Sprintf("existing %s %s value : %s", kind, k, valueDisplay))
// 	}
// }
