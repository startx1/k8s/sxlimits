# sxlimits default template

Template designed for a default limitRange definition

## Usage

```bash
sxlimits create default-limits default
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: default-limits
spec:
  limits:
  - type: Container
    default:
      cpu: 200m
      ephemeral-storage: 4Gi
      memory: 256Mi
    defaultRequest:
      cpu: 100m
      ephemeral-storage: 4Gi
      memory: 128Mi
```
