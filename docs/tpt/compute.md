# sxlimits compute template

Template designed for a compute limitRange definition

## Usage

```bash
sxlimits create cpu-limits compute
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: cpu-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
    min:
      cpu: 20m
  - type: Container
    default:
      cpu: 200m
    defaultRequest:
      cpu: 100m
    max:
      cpu: "6"
    min:
      cpu: 20m
```
