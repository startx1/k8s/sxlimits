/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package limitrange

import (
	"context"
	"fmt"
	"strconv"

	sxIncrementor "gitlab.com/startx1/k8s/go-libs/pkg/incrementor"
	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxTpl "gitlab.com/startx1/k8s/sxlimits/pkg/templates"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlimits.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName = "limitrange"
	// Warn and error messages
	warnErrorIncrement           = "limit %s with value %s could not increment with %s : %v"
	errLoadErr                   = "limitRange '%s' in namespace '%s' has an error : %v"
	errLoadFound                 = "limitRange '%s' in namespace '%s' is not found : %v"
	errMergeTplFound             = "error getting template : %v"
	errMergeTplFoundQ            = "failed to get destLimitRange: %s in %s : %v"
	errApplyExec                 = "failed to apply LimitRange '%s' in namespace '%s' update to the cluster : %v"
	errDetectMinMaxFromPod       = "error getting all pods in namespace '%s' : %v"
	errDetectMinMaxFromContainer = "error getting all containers in namespace '%s' : %v"
	// Debug messages
	ddInitNewLR                           = "Init LimitRange object"
	ddInitnewLR                           = "Init a new LimitRange object"
	ddInitLoad                            = "Load the limitRanges %s from the cluster"
	ddInitResize                          = "Resize the limitRanges %s to %s (scope: %s)"
	ddInitAdjust                          = "Adjust the limitRanges %s to %s (scope: %s)"
	ddInitMergeTpl                        = "Merge %s template into the %s LimitRange (obj)"
	ddInitApply                           = "Apply the limitRanges %s in namespace %s the cluster"
	ddInitGetYaml                         = "Return yaml content of the limitRanges %s in namespace %s"
	ddInitPrint                           = "Print content of the limitRanges %s in namespace %s"
	ddInitincrementLimitRangeValues       = "Increments the limitRanges list"
	ddIncrementRV                         = "Increment from %s to %s for %s"
	ddMergeTplAddVal                      = "Adding %s with value %s into the %s LimitRange"
	ddDebugQtStart                        = "DEBUG the limitRange"
	ddDebugQtLimitRange                   = "DEBUG - limitRange resource UUID         : %s"
	ddDebugQtValName                      = "DEBUG - limitRange name                  : %s"
	ddDebugQtValNamespace                 = "DEBUG - limitRange namespace             : %s"
	ddDebugQtValIsLoaded                  = "DEBUG - limitRange is loaded             : %t"
	ddDebugQtValLimitRangeRule            = "DEBUG - limitRange define a limit for %s : %s"
	ddMergeTplScanType                    = "Start scanning template limit %s"
	ddMergeTplTypeCreate                  = "template limit %s not found in destination, creating it"
	ddMergeTplScanTypeCat                 = "Start scanning template limit category (Min, Max, Default, ...) for %s type"
	ddMergeTplTypeCatCreate               = "template limit category %s/%s not found in destination, creating it"
	ddMergeTplScanTypeCatResources        = "Start scanning template limit for %s type"
	ddMergeTplScanTypeCatResource         = "Start scanning template limit for %s type, %s category and %s resource with val %s"
	ddMergeTplScanTypeCatResourceCreate   = "template limit resource %s/%s/%s not found in destination, creating it with val %s"
	ddMergeTplScanTypeCatResourceUpdate   = "template limit resource %s/%s/%s found with lower value, updating it with val %s"
	ddMergeTplScanTypeCatResourceNoUpdate = "template limit resource %s/%s/%s found with higher value, nothing to do"
	ddInitAdjustAllForContainer           = "Init ddInitAdjustAllForContainer (increment %s, scope %s)"
	ddInitAdjustAllForPod                 = "Init ddInitAdjustAllForPod (increment %s, scope %s)"
	ddInitAdjustAllForTypeCategory        = "Init AdjustAllForTypeCategory %s - %s (increment %s)"
	ddInitIncrementAllForType             = "Init IncrementAllForType %s (increment %s, scope %s)"
	ddInitAdjustAllForType                = "Init AdjustAllForType %s (increment %s)"
	ddInitIncrementAllForTypeCategory     = "Init IncrementAllForTypeCategory %s - %s (increment %s)"
	ddInitDetectMinMaxFromPod             = "MinMax : Start Detect POD MinMax in namespace %s"
	ddInitDetectMinMaxFromContainer       = "MinMax : Start Detect CONTAINER MinMax in namespace %s"
	ddInitDetectMinMaxChecking            = "MinMax : - Checking pod %s in namespace %s"
	ddInitDetectMinMaxUpdateMin           = "MinMax :   - %s - %s : Minimum = %s"
	ddInitDetectMinMaxUpdateMax           = "MinMax :   - %s - %s : Maximum = %s"
)

//
// Definitions of the LimitRanges object
//

// LimitRanges represent the object properties
type LimitRanges struct {
	k8sclient  *sxKCli.K8sClient
	LimitRange *v1.LimitRange
	Name       string
	Namespace  string
	IsLoaded   bool
	display    *sxUtils.CmdDisplay
}

// NewLRs create a new LimitRanges ready to use the clientset
// to interact with the kubernetes cluster
func NewLR(name string, namespace string, k8sclient *sxKCli.K8sClient) *LimitRanges {
	// sxUtils.DisplayDebug(GroupName,ddInitNewLR)
	limitRange := newLR(name, namespace)
	return &LimitRanges{
		k8sclient:  k8sclient,
		Name:       name,
		Namespace:  namespace,
		LimitRange: limitRange,
		IsLoaded:   false,
		display:    sxUtils.NewCmdDisplay(GroupNameDef),
	}
}

// newLR create a new LimitRangeSpec with name and namespace set
// to the given parameters. everitying else is default
func newLR(name string, namespace string) *v1.LimitRange {
	// display := sxUtils.NewCmdDisplay(GroupName)
	// display.Debug(ddInitnewLR)
	// Define the LimitRange spec.
	limitRangeSpec := v1.LimitRangeSpec{
		Limits: []v1.LimitRangeItem{
			{
				Type: v1.LimitTypePod,
				Max: map[v1.ResourceName]resource.Quantity{
					v1.ResourceCPU:    resource.MustParse("1"),
					v1.ResourceMemory: resource.MustParse("2Gi"),
				},
				Default: map[v1.ResourceName]resource.Quantity{
					v1.ResourceCPU:    resource.MustParse("500m"),
					v1.ResourceMemory: resource.MustParse("500Mi"),
				},
			},
		},
	}

	// Define the LimitRange object
	limitRange := &v1.LimitRange{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: limitRangeSpec,
	}
	return limitRange
}

// Load load the limitRanges %s from the cluster
// or return an error
func (rq *LimitRanges) Load() error {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug(fmt.Sprintf(ddInitLoad, rq.Name))
	limitrange, err := rq.k8sclient.Clientset.CoreV1().LimitRanges(rq.Namespace).Get(
		context.TODO(),
		rq.Name,
		metav1.GetOptions{},
	)
	if err != nil {
		messageErr := errLoadErr
		if errors.IsNotFound(err) {
			messageErr = errLoadFound
		}
		return fmt.Errorf(
			messageErr,
			rq.Name,
			rq.Namespace,
			err,
		)
	}
	rq.LimitRange = limitrange
	rq.IsLoaded = true
	return nil
}

// Resize increment or decrement the Spec.Hard accroding to the increment value.
// Default is 0 and return an error if something is wrong
func (rq *LimitRanges) Resize(incrementValue string, scope string) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitResize,
			rq.Name,
			incrementValue,
			scope,
		),
	)
	if !rq.IsLoaded {
		errLoad := rq.Load()
		if errLoad != nil {
			return errLoad
		}
	}

	incr := sxIncrementor.NewIncrementor(incrementValue)
	for _, limitsDest := range rq.LimitRange.Spec.Limits {
		rq.resizeAllForType(string(limitsDest.Type), incr, scope)
	}

	return nil
}

// Adjust copy all the Status.Used to the Spec.Hard and
// increment or decrement accroding to the increment value. Default
// is 0 and return an error if something is wrong
func (rq *LimitRanges) Adjust(incrementValue string, scope string) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitAdjust,
			rq.Name,
			incrementValue,
			scope,
		),
	)
	if !rq.IsLoaded {
		errLoad := rq.Load()
		if errLoad != nil {
			return errLoad
		}
	}

	adjustMatrixPod, errMatrixPod := rq.DetectMinMaxFromPod()
	if errMatrixPod != nil {
		return errMatrixPod
	}
	adjustMatrixContainer, errMatrixContainer := rq.DetectMinMaxFromContainer()
	if errMatrixContainer != nil {
		return errMatrixContainer
	}

	incr := sxIncrementor.NewIncrementor(incrementValue)
	for _, limitsDest := range rq.LimitRange.Spec.Limits {
		limitType := string(limitsDest.Type)
		if limitType == "Container" {
			switch scope {
			case "all",
				"container",
				"minmax",
				"default",
				"ratio",
				"min",
				"max":
				rq.adjustAllForContainer(incr, adjustMatrixContainer, scope)
			}
		} else if limitType == "Pod" {
			switch scope {
			case "all",
				"pod",
				"minmax",
				"default",
				"ratio",
				"min",
				"max":
				rq.adjustAllForPod(incr, adjustMatrixPod, scope)
			}
		}
	}
	return nil
}

// Merge content from a template into the current limitRange
func (rq *LimitRanges) MergeTemplate(template sxTpl.Template) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitMergeTpl,
			template.GetName(),
			rq.Name,
		),
	)
	templateLimitRange, err := template.GetContentObj()

	if err != nil {
		return fmt.Errorf(errMergeTplFound, err)
	}
	// Get the existing destLimitRange from the Kubernetes API.
	destLimitRange, errGet := rq.
		k8sclient.
		Clientset.
		CoreV1().
		LimitRanges(rq.Namespace).
		Get(context.TODO(),
			rq.Name,
			metav1.GetOptions{},
		)
	if errGet != nil {
		return fmt.Errorf(
			errMergeTplFoundQ,
			rq.Name,
			rq.Namespace,
			errGet,
		)
	}
	// Merge the LimitRanges.
	for _, limitsTpl := range templateLimitRange.Spec.Limits {
		rq.display.Debug(
			fmt.Sprintf(
				ddMergeTplScanType,
				limitsTpl.Type,
			),
		)
		foundType := false
		// scan destination and add missing type
		for _, limitsDest := range destLimitRange.Spec.Limits {
			if limitsTpl.Type == limitsDest.Type {
				foundType = true
			}
		}
		if !foundType {
			rq.display.Debug(
				fmt.Sprintf(
					ddMergeTplTypeCreate,
					limitsTpl.Type,
				),
			)
			newLimitRangeItem := v1.LimitRangeItem{
				Type: limitsTpl.Type,
			}
			destLimitRange.Spec.Limits = append(destLimitRange.Spec.Limits, newLimitRangeItem)
		}

		// scan destination and for each type add missing Min, Max, Default, DefaultRequest and MaxLimitRequestRatio
		for limitsDestPos, limitsDest := range destLimitRange.Spec.Limits {
			if limitsTpl.Type == limitsDest.Type {
				rq.display.Debug(
					fmt.Sprintf(
						ddMergeTplScanTypeCat,
						limitsTpl.Type,
					),
				)
				if limitsTpl.Min != nil && limitsDest.Min == nil {
					rq.display.Debug(
						fmt.Sprintf(
							ddMergeTplTypeCatCreate,
							limitsTpl.Type,
							"Min",
						),
					)
					destLimitRange.Spec.Limits[limitsDestPos].Min = make(map[v1.ResourceName]resource.Quantity)
				}
				if limitsTpl.Max != nil && limitsDest.Max == nil {
					rq.display.Debug(
						fmt.Sprintf(
							ddMergeTplTypeCatCreate,
							limitsTpl.Type,
							"Max",
						),
					)
					destLimitRange.Spec.Limits[limitsDestPos].Max = make(map[v1.ResourceName]resource.Quantity)
				}
				if limitsTpl.DefaultRequest != nil && limitsDest.DefaultRequest == nil {
					rq.display.Debug(
						fmt.Sprintf(
							ddMergeTplTypeCatCreate,
							limitsTpl.Type,
							"DefaultRequest",
						),
					)
					destLimitRange.Spec.Limits[limitsDestPos].DefaultRequest = make(map[v1.ResourceName]resource.Quantity)
				}
				if limitsTpl.Default != nil && limitsDest.Default == nil {
					rq.display.Debug(
						fmt.Sprintf(
							ddMergeTplTypeCatCreate,
							limitsTpl.Type,
							"Default",
						),
					)
					destLimitRange.Spec.Limits[limitsDestPos].Default = make(map[v1.ResourceName]resource.Quantity)
				}
				if limitsTpl.MaxLimitRequestRatio != nil && limitsDest.MaxLimitRequestRatio == nil {
					rq.display.Debug(
						fmt.Sprintf(
							ddMergeTplTypeCatCreate,
							limitsTpl.Type,
							"MaxLimitRequestRatio",
						),
					)
					destLimitRange.Spec.Limits[limitsDestPos].MaxLimitRequestRatio = make(map[v1.ResourceName]resource.Quantity)
				}
			}
		}

		// scan destination and for each type add missing key
		for limitsDestPos, limitsDest := range destLimitRange.Spec.Limits {
			if limitsTpl.Type == limitsDest.Type {
				rq.display.Debug(
					fmt.Sprintf(
						ddMergeTplScanTypeCatResources,
						limitsTpl.Type,
					),
				)
				// analyse Min entries
				if limitsTpl.Min != nil {
					for limitsTplName, limitsTplQty := range limitsTpl.Min {
						rq.display.Debug(
							fmt.Sprintf(
								ddMergeTplScanTypeCatResource,
								limitsTpl.Type,
								"Min",
								limitsTplName,
								limitsTplQty.String(),
							),
						)
						if limitsDestQty, ok := limitsDest.Min[limitsTplName]; ok {
							limitsTplDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsTplQty.String(),
								false,
							)
							limitsDestDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsDestQty.String(),
								false,
							)
							if limitsTplDef.GetValInt() > limitsDestDef.GetValInt() {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceUpdate,
										limitsTpl.Type,
										"Min",
										limitsTplName,
										limitsTplQty.String(),
									),
								)
								destLimitRange.
									Spec.
									Limits[limitsDestPos].
									Min[limitsTplName] = limitsTplQty
							} else {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceNoUpdate,
										limitsTpl.Type,
										"Min",
										limitsTplName,
									),
								)
							}
						} else {
							rq.display.Debug(
								fmt.Sprintf(
									ddMergeTplScanTypeCatResourceCreate,
									limitsTpl.Type,
									"Min",
									limitsTplName,
									limitsTplQty.String(),
								),
							)
							destLimitRange.
								Spec.
								Limits[limitsDestPos].
								Min[limitsTplName] = limitsTplQty
						}
					}
				}
				// analyse Max entries
				if limitsTpl.Max != nil {
					for limitsTplName, limitsTplQty := range limitsTpl.Max {
						rq.display.Debug(
							fmt.Sprintf(
								ddMergeTplScanTypeCatResource,
								limitsTpl.Type,
								"Max",
								limitsTplName,
								limitsTplQty.String(),
							),
						)
						if limitsDestQty, ok := limitsDest.Max[limitsTplName]; ok {
							limitsTplDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsTplQty.String(),
								false,
							)
							limitsDestDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsDestQty.String(),
								false,
							)
							if limitsTplDef.GetValInt() > limitsDestDef.GetValInt() {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceUpdate,
										limitsTpl.Type,
										"Max",
										limitsTplName,
										limitsTplQty.String(),
									),
								)
								destLimitRange.
									Spec.
									Limits[limitsDestPos].
									Max[limitsTplName] = limitsTplQty
							} else {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceNoUpdate,
										limitsTpl.Type,
										"Min",
										limitsTplName,
									),
								)
							}
						} else {
							rq.display.Debug(
								fmt.Sprintf(
									ddMergeTplScanTypeCatResourceCreate,
									limitsTpl.Type,
									"Max",
									limitsTplName,
									limitsTplQty.String(),
								),
							)
							destLimitRange.
								Spec.
								Limits[limitsDestPos].
								Max[limitsTplName] = limitsTplQty
						}
					}
				}
				// analyse DefaultRequest entries
				if limitsTpl.DefaultRequest != nil {
					for limitsTplName, limitsTplQty := range limitsTpl.DefaultRequest {
						rq.display.Debug(
							fmt.Sprintf(
								ddMergeTplScanTypeCatResource,
								limitsTpl.Type,
								"DefaultRequest",
								limitsTplName,
								limitsTplQty.String(),
							),
						)
						if limitsDestQty, ok := limitsDest.DefaultRequest[limitsTplName]; ok {
							limitsTplDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsTplQty.String(),
								false,
							)
							limitsDestDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsDestQty.String(),
								false,
							)
							if limitsTplDef.GetValInt() > limitsDestDef.GetValInt() {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceUpdate,
										limitsTpl.Type,
										"DefaultRequest",
										limitsTplName,
										limitsTplQty.String(),
									),
								)
								destLimitRange.
									Spec.
									Limits[limitsDestPos].
									DefaultRequest[limitsTplName] = limitsTplQty
							} else {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceNoUpdate,
										limitsTpl.Type,
										"Min",
										limitsTplName,
									),
								)
							}
						} else {
							rq.display.Debug(
								fmt.Sprintf(
									ddMergeTplScanTypeCatResourceCreate,
									limitsTpl.Type,
									"DefaultRequest",
									limitsTplName,
									limitsTplQty.String(),
								),
							)
							destLimitRange.
								Spec.
								Limits[limitsDestPos].
								DefaultRequest[limitsTplName] = limitsTplQty
						}
					}
				}
				// analyse Default entries
				if limitsTpl.Default != nil {
					for limitsTplName, limitsTplQty := range limitsTpl.Default {
						rq.display.Debug(
							fmt.Sprintf(
								ddMergeTplScanTypeCatResource,
								limitsTpl.Type,
								"Default",
								limitsTplName,
								limitsTplQty.String(),
							),
						)
						if limitsDestQty, ok := limitsDest.Default[limitsTplName]; ok {
							limitsTplDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsTplQty.String(),
								false,
							)
							limitsDestDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsDestQty.String(),
								false,
							)
							if limitsTplDef.GetValInt() > limitsDestDef.GetValInt() {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceUpdate,
										limitsTpl.Type,
										"Default",
										limitsTplName,
										limitsTplQty.String(),
									),
								)
								destLimitRange.
									Spec.
									Limits[limitsDestPos].
									Default[limitsTplName] = limitsTplQty
							} else {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceNoUpdate,
										limitsTpl.Type,
										"Min",
										limitsTplName,
									),
								)
							}
						} else {
							rq.display.Debug(
								fmt.Sprintf(
									ddMergeTplScanTypeCatResourceCreate,
									limitsTpl.Type,
									"Default",
									limitsTplName,
									limitsTplQty.String(),
								),
							)
							destLimitRange.
								Spec.
								Limits[limitsDestPos].
								Default[limitsTplName] = limitsTplQty
						}
					}
				}
				// analyse MaxLimitRequestRatio entries
				if limitsTpl.MaxLimitRequestRatio != nil {
					for limitsTplName, limitsTplQty := range limitsTpl.MaxLimitRequestRatio {
						rq.display.Debug(
							fmt.Sprintf(
								ddMergeTplScanTypeCatResource,
								limitsTpl.Type,
								"MaxLimitRequestRatio",
								limitsTplName,
								limitsTplQty.String(),
							),
						)
						if limitsDestQty, ok := limitsDest.MaxLimitRequestRatio[limitsTplName]; ok {
							limitsTplDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsTplQty.String(),
								false,
							)
							limitsDestDef := NewLimitRangeDef(
								limitsTplName.String(),
								limitsDestQty.String(),
								false,
							)
							if limitsTplDef.GetValInt() > limitsDestDef.GetValInt() {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceUpdate,
										limitsTpl.Type,
										"MaxLimitRequestRatio",
										limitsTplName,
										limitsTplQty.String(),
									),
								)
								destLimitRange.
									Spec.
									Limits[limitsDestPos].
									MaxLimitRequestRatio[limitsTplName] = limitsTplQty
							} else {
								rq.display.Debug(
									fmt.Sprintf(
										ddMergeTplScanTypeCatResourceNoUpdate,
										limitsTpl.Type,
										"Min",
										limitsTplName,
									),
								)
							}
						} else {
							rq.display.Debug(
								fmt.Sprintf(
									ddMergeTplScanTypeCatResourceCreate,
									limitsTpl.Type,
									"MaxLimitRequestRatio",
									limitsTplName,
									limitsTplQty.String(),
								),
							)
							destLimitRange.
								Spec.
								Limits[limitsDestPos].
								MaxLimitRequestRatio[limitsTplName] = limitsTplQty
						}
					}
				}
			}
		}
	}
	rq.LimitRange = destLimitRange
	return nil
}

// Resize copy all the Status.Used to the Spec.Hard and
// increment or decrement accroding to the increment value. Default
// is 0 and return an error if something is wrong
func (rq *LimitRanges) Apply() error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitApply,
			rq.Name,
			rq.Namespace,
		),
	)
	_, err := rq.
		k8sclient.
		Clientset.
		CoreV1().
		LimitRanges(rq.Namespace).
		Update(
			context.TODO(),
			rq.LimitRange,
			metav1.UpdateOptions{},
		)
	if err != nil {
		return fmt.Errorf(
			errApplyExec,
			rq.Name,
			rq.Namespace,
			err,
		)
	}
	return nil
}

// GetYaml return a YAML representation of the LimitRange
func (rq *LimitRanges) GetYaml() string {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitGetYaml,
			rq.Name,
			rq.Namespace,
		),
	)
	yamlData, err := yaml.Marshal(&rq.LimitRange)
	if err != nil {
		return ""
	}
	return string(yamlData)
}

// GetYaml return a YAML representation of the LimitRange
func (rq *LimitRanges) Print() {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitPrint,
			rq.Name,
			rq.Namespace,
		),
	)
	fmt.Println(rq.GetYaml())
}

// Get gets the v1.LimitRange from the LimitRanges.
func (rq *LimitRanges) Get() *v1.LimitRange {
	return rq.LimitRange
}

// GetName gets the name from the LimitRanges.
func (rq *LimitRanges) GetName() string {
	return rq.Name
}

// GetNamespace gets the namespace from the LimitRanges.
func (rq *LimitRanges) GetNamespace() string {
	return rq.Namespace
}

// GetNamespace gets the namespace from the LimitRanges.
func (rq *LimitRanges) Debug() *LimitRanges {
	rq.display.Debug(ddDebugQtStart)
	rq.display.Debug(fmt.Sprintf(ddDebugQtLimitRange, rq.LimitRange.GetUID()))
	rq.display.Debug(fmt.Sprintf(ddDebugQtValName, rq.GetName()))
	rq.display.Debug(fmt.Sprintf(ddDebugQtValNamespace, rq.GetNamespace()))
	rq.display.Debug(fmt.Sprintf(ddDebugQtValIsLoaded, rq.IsLoaded))
	// rq.Print()
	return rq
}

func (rq *LimitRanges) adjustAllForContainer(increment *sxIncrementor.Incrementor, adjustMatrix map[string]map[string]map[string]*LimitRangeDef, scope string) error {
	limitType := "Container"
	rq.display.Debug(
		fmt.Sprintf(
			ddInitAdjustAllForContainer,
			increment.GetValStringUnit(),
			scope,
		),
	)

	for _, limitsDest := range rq.LimitRange.Spec.Limits {
		if string(limitsDest.Type) == limitType {
			if limitsDest.Min != nil {
				switch scope {
				case "all",
					"container",
					"minmax",
					"min":
					rq.adjustAllForTypeCategory(limitType, "Min", increment, adjustMatrix)
				}
			}
			if limitsDest.Max != nil {
				switch scope {
				case "all",
					"container",
					"minmax",
					"max":
					rq.adjustAllForTypeCategory(limitType, "Max", increment, adjustMatrix)
				}
			}
			if limitsDest.Default != nil {
				switch scope {
				case "all",
					"container",
					"default",
					"max":
					rq.adjustAllForTypeCategory(limitType, "Default", increment, adjustMatrix)
				}
			}
			if limitsDest.DefaultRequest != nil {
				switch scope {
				case "all",
					"container",
					"default",
					"min":
					rq.adjustAllForTypeCategory(limitType, "DefaultRequest", increment, adjustMatrix)
				}
			}
			if limitsDest.MaxLimitRequestRatio != nil {
				switch scope {
				case "all",
					"container",
					"ratio":
					rq.adjustAllForTypeCategory(limitType, "MaxLimitRequestRatio", increment, adjustMatrix)
				}
			}
		}
	}
	return nil
}

func (rq *LimitRanges) adjustAllForPod(increment *sxIncrementor.Incrementor, adjustMatrix map[string]map[string]map[string]*LimitRangeDef, scope string) error {
	limitType := "Pod"
	rq.display.Debug(
		fmt.Sprintf(
			ddInitAdjustAllForPod,
			increment.GetValStringUnit(),
			scope,
		),
	)
	for _, limitsDest := range rq.LimitRange.Spec.Limits {
		if string(limitsDest.Type) == limitType {
			if limitsDest.Min != nil {
				switch scope {
				case "all",
					"container",
					"minmax",
					"min":
					rq.adjustAllForTypeCategory(limitType, "Min", increment, adjustMatrix)
				}
			}
			if limitsDest.Max != nil {
				switch scope {
				case "all",
					"container",
					"minmax",
					"max":
					rq.adjustAllForTypeCategory(limitType, "Max", increment, adjustMatrix)
				}
			}
			if limitsDest.Default != nil {
				switch scope {
				case "all",
					"container",
					"default",
					"max":
					rq.adjustAllForTypeCategory(limitType, "Default", increment, adjustMatrix)
				}
			}
			if limitsDest.DefaultRequest != nil {
				switch scope {
				case "all",
					"container",
					"default",
					"min":
					rq.adjustAllForTypeCategory(limitType, "DefaultRequest", increment, adjustMatrix)
				}
			}
			if limitsDest.MaxLimitRequestRatio != nil {
				switch scope {
				case "all",
					"container",
					"ratio":
					rq.adjustAllForTypeCategory(limitType, "MaxLimitRequestRatio", increment, adjustMatrix)
				}
			}
		}
	}
	return nil
}

func (rq *LimitRanges) adjustAllForTypeCategory(limitType string, limitCategory string, increment *sxIncrementor.Incrementor, adjustMatrix map[string]map[string]map[string]*LimitRangeDef) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitAdjustAllForTypeCategory,
			limitType,
			limitCategory,
			increment.GetValStringUnit(),
		),
	)

	incrementMin := sxIncrementor.NewIncrementor(increment.GetValStringUnit())
	for limitDestNum, limitsDest := range rq.LimitRange.Spec.Limits {
		if string(limitsDest.Type) == limitType {
			switch limitCategory {
			case "Min":
				for limitsDestName, limitsDestQty := range limitsDest.Min {
					newLimitRangeRef := adjustMatrix[string(limitsDestName)]["request"]["min"]
					var errorIncr error
					if increment.IsPercentage() && increment.GetValInt() > 100 {
						errorIncr = newLimitRangeRef.Decrement(incrementMin)
					} else {
						errorIncr = newLimitRangeRef.Increment(increment)
					}
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].Min[limitsDestName] = rQTT
						}
					}
				}
			case "Max":
				for limitsDestName, limitsDestQty := range limitsDest.Max {
					newLimitRangeRef := adjustMatrix[string(limitsDestName)]["limit"]["max"]
					errorIncr := newLimitRangeRef.Increment(increment)
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].Max[limitsDestName] = rQTT
						}
					}
				}
			case "Default":
				for limitsDestName, limitsDestQty := range limitsDest.Default {
					newLimitRangeRef := adjustMatrix[string(limitsDestName)]["request"]["min"]
					var errorIncr error
					if increment.IsPercentage() && increment.GetValInt() > 100 {
						errorIncr = newLimitRangeRef.Decrement(incrementMin)
					} else {
						errorIncr = newLimitRangeRef.Increment(increment)
					}
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].Default[limitsDestName] = rQTT
						}
					}
				}
			case "DefaultRequest":
				for limitsDestName, limitsDestQty := range limitsDest.DefaultRequest {
					newLimitRangeRef := adjustMatrix[string(limitsDestName)]["request"]["min"]
					errorIncr := newLimitRangeRef.Decrement(incrementMin)
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].DefaultRequest[limitsDestName] = rQTT
						}
					}
				}
			case "MaxLimitRequestRatio":
				for limitsDestName, limitsDestQty := range limitsDest.MaxLimitRequestRatio {
					newLimitRangeRef := adjustMatrix[string(limitsDestName)]["request"]["max"]
					errorIncr := newLimitRangeRef.Increment(increment)
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].MaxLimitRequestRatio[limitsDestName] = rQTT
						}
					}
				}
			}
		}
	}
	return nil
}

func (rq *LimitRanges) resizeAllForType(limitType string, increment *sxIncrementor.Incrementor, scope string) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitIncrementAllForType,
			limitType,
			increment.GetValStringUnit(),
			scope,
		),
	)
	for _, limitsDest := range rq.LimitRange.Spec.Limits {
		if string(limitsDest.Type) == limitType {
			if limitsDest.Min != nil {
				switch scope {
				case "all",
					"container",
					"minmax",
					"min":
					rq.resizeAllForTypeCategory(limitType, "Min", increment)
				}
			}
			if limitsDest.Max != nil {
				switch scope {
				case "all",
					"container",
					"minmax",
					"max":
					rq.resizeAllForTypeCategory(limitType, "Max", increment)
				}
			}
			if limitsDest.Default != nil {
				switch scope {
				case "all",
					"container",
					"default",
					"max":
					rq.resizeAllForTypeCategory(limitType, "Default", increment)
				}
			}
			if limitsDest.DefaultRequest != nil {
				switch scope {
				case "all",
					"container",
					"default",
					"min":
					rq.resizeAllForTypeCategory(limitType, "DefaultRequest", increment)
				}
			}
			if limitsDest.MaxLimitRequestRatio != nil {
				switch scope {
				case "all",
					"container",
					"ratio":
					rq.resizeAllForTypeCategory(limitType, "MaxLimitRequestRatio", increment)
				}
			}
		}
	}
	return nil
}

func (rq *LimitRanges) resizeAllForTypeCategory(limitType string, limitCategory string, increment *sxIncrementor.Incrementor) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitIncrementAllForTypeCategory,
			limitType,
			limitCategory,
			increment.GetValStringUnit(),
		),
	)
	incrementMin := sxIncrementor.NewIncrementor(increment.GetValStringUnit())
	for limitDestNum, limitsDest := range rq.LimitRange.Spec.Limits {
		if string(limitsDest.Type) == limitType {
			switch limitCategory {
			case "Min":
				for limitsDestName, limitsDestQty := range limitsDest.Min {
					newLimitRangeRef := NewLimitRangeDef(
						string(limitsDestName),
						limitsDestQty.String(),
						false,
					)
					var errorIncr error
					if increment.IsPercentage() && increment.GetValInt() > 100 {
						errorIncr = newLimitRangeRef.Decrement(incrementMin)
					} else {
						errorIncr = newLimitRangeRef.Increment(increment)
					}
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].Min[limitsDestName] = rQTT
						}
					}
				}
			case "Max":
				for limitsDestName, limitsDestQty := range limitsDest.Max {
					newLimitRangeRef := NewLimitRangeDef(
						string(limitsDestName),
						limitsDestQty.String(),
						false,
					)
					errorIncr := newLimitRangeRef.Increment(increment)
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].Max[limitsDestName] = rQTT
						}
					}
				}
			case "Default":
				for limitsDestName, limitsDestQty := range limitsDest.Default {
					newLimitRangeRef := NewLimitRangeDef(
						string(limitsDestName),
						limitsDestQty.String(),
						false,
					)
					errorIncr := newLimitRangeRef.Increment(increment)
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].Default[limitsDestName] = rQTT
						}
					}
				}
			case "DefaultRequest":
				for limitsDestName, limitsDestQty := range limitsDest.DefaultRequest {
					newLimitRangeRef := NewLimitRangeDef(
						string(limitsDestName),
						limitsDestQty.String(),
						false,
					)
					errorIncr := newLimitRangeRef.Increment(increment)
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].DefaultRequest[limitsDestName] = rQTT
						}
					}
				}
			case "MaxLimitRequestRatio":
				for limitsDestName, limitsDestQty := range limitsDest.MaxLimitRequestRatio {
					newLimitRangeRef := NewLimitRangeDef(
						string(limitsDestName),
						limitsDestQty.String(),
						false,
					)
					errorIncr := newLimitRangeRef.Increment(increment)
					if errorIncr != nil {
						rq.display.Warning(
							fmt.Sprintf(
								warnErrorIncrement,
								string(limitsDestName),
								limitsDestQty.String(),
								increment.GetValStringUnit(),
								errorIncr,
							),
						)
					} else {
						newValue := *newLimitRangeRef.GetValQty()
						var rQTT resource.Quantity
						var err error
						switch newLimitRangeRef.GetValUnit() {
						case "m":
							rQTT, err = resource.ParseQuantity(strconv.Itoa(int(newValue.Value())) + newLimitRangeRef.GetValUnit())
						case "Ki", "Mi", "Gi", "Ti", "Pi":
							rQTT, err = resource.ParseQuantity(newValue.String() + newLimitRangeRef.GetValUnit())
						default:
							rQTT, err = resource.ParseQuantity(newValue.String())
						}
						if err != nil {
							rq.display.Warning(
								fmt.Sprintf(
									warnErrorIncrement,
									string(limitsDestName),
									limitsDestQty.String(),
									increment.GetValStringUnit(),
									errorIncr,
								),
							)
						} else {
							rq.LimitRange.Spec.Limits[limitDestNum].MaxLimitRequestRatio[limitsDestName] = rQTT
						}
					}
				}
			}
		}
	}
	return nil
}

func (rq *LimitRanges) DetectMinMaxFromPod() (map[string]map[string]map[string]*LimitRangeDef, error) {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitDetectMinMaxFromPod,
			rq.Namespace,
		),
	)
	pods, err := rq.k8sclient.Clientset.CoreV1().Pods(rq.Namespace).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, fmt.Errorf(
			errDetectMinMaxFromPod,
			rq.Namespace,
			err,
		)
	}

	// Initialize variables to track min and max values
	matrix := make(map[string]map[string]map[string]*LimitRangeDef)
	matrix["cpu"] = make(map[string]map[string]*LimitRangeDef)
	matrix["memory"] = make(map[string]map[string]*LimitRangeDef)
	matrix["ephemeral-storage"] = make(map[string]map[string]*LimitRangeDef)

	// Fill the matrix with CPU, memory, and ephemeral-storage requests and limits
	for _, pod := range pods.Items {
		rq.display.Debug(
			fmt.Sprintf(
				ddInitDetectMinMaxChecking,
				pod.Name,
				pod.Namespace,
			),
		)
		for _, container := range pod.Spec.Containers {
			// CPU
			cpuRequestRes := container.Resources.Requests[v1.ResourceCPU]
			cpuRequest := NewLimitRangeDef("cpu", cpuRequestRes.String(), false)
			cpuLimitRes := container.Resources.Limits[v1.ResourceCPU]
			cpuLimit := NewLimitRangeDef("cpu", cpuLimitRes.String(), false)
			rq.updateMinMax("cpu", "request", matrix, cpuRequest)
			rq.updateMinMax("cpu", "limit", matrix, cpuLimit)

			// Memory
			memoryRequestRes := container.Resources.Requests[v1.ResourceMemory]
			memoryRequest := NewLimitRangeDef("memory", memoryRequestRes.String(), false)
			memoryLimitRes := container.Resources.Limits[v1.ResourceMemory]
			memoryLimit := NewLimitRangeDef("memory", memoryLimitRes.String(), false)
			rq.updateMinMax("memory", "request", matrix, memoryRequest)
			rq.updateMinMax("memory", "limit", matrix, memoryLimit)

			// Ephemeral storage
			storageRequestRes := container.Resources.Requests[v1.ResourceEphemeralStorage]
			storageRequest := NewLimitRangeDef("ephemeral-storage", storageRequestRes.String(), false)
			storageLimitRes := container.Resources.Limits[v1.ResourceEphemeralStorage]
			storageLimit := NewLimitRangeDef("ephemeral-storage", storageLimitRes.String(), false)
			rq.updateMinMax("ephemeral-storage", "request", matrix, storageRequest)
			rq.updateMinMax("ephemeral-storage", "limit", matrix, storageLimit)
		}
	}
	return matrix, nil
}

func (rq *LimitRanges) DetectMinMaxFromContainer() (map[string]map[string]map[string]*LimitRangeDef, error) {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitDetectMinMaxFromContainer,
			rq.Namespace,
		),
	)
	pods, err := rq.k8sclient.Clientset.CoreV1().Pods(rq.Namespace).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, fmt.Errorf(
			errDetectMinMaxFromContainer,
			rq.Namespace,
			err,
		)
	}

	// Initialize variables to track min and max values
	matrix := make(map[string]map[string]map[string]*LimitRangeDef)
	matrix["cpu"] = make(map[string]map[string]*LimitRangeDef)
	matrix["memory"] = make(map[string]map[string]*LimitRangeDef)
	matrix["ephemeral-storage"] = make(map[string]map[string]*LimitRangeDef)

	// Fill the matrix with CPU, memory, and ephemeral-storage requests and limits
	for _, pod := range pods.Items {
		for _, container := range pod.Spec.Containers {
			// CPU
			cpuRequestRes := container.Resources.Requests[v1.ResourceCPU]
			cpuRequest := NewLimitRangeDef("cpu", cpuRequestRes.String(), false)
			cpuLimitRes := container.Resources.Limits[v1.ResourceCPU]
			cpuLimit := NewLimitRangeDef("cpu", cpuLimitRes.String(), false)
			rq.updateMinMax("cpu", "request", matrix, cpuRequest)
			rq.updateMinMax("cpu", "limit", matrix, cpuLimit)

			// Memory
			memoryRequestRes := container.Resources.Requests[v1.ResourceMemory]
			memoryRequest := NewLimitRangeDef("memory", memoryRequestRes.String(), false)
			memoryLimitRes := container.Resources.Limits[v1.ResourceMemory]
			memoryLimit := NewLimitRangeDef("memory", memoryLimitRes.String(), false)
			rq.updateMinMax("memory", "request", matrix, memoryRequest)
			rq.updateMinMax("memory", "limit", matrix, memoryLimit)

			// Ephemeral storage
			storageRequestRes := container.Resources.Requests[v1.ResourceEphemeralStorage]
			storageRequest := NewLimitRangeDef("ephemeral-storage", storageRequestRes.String(), false)
			storageLimitRes := container.Resources.Limits[v1.ResourceEphemeralStorage]
			storageLimit := NewLimitRangeDef("ephemeral-storage", storageLimitRes.String(), false)
			rq.updateMinMax("ephemeral-storage", "request", matrix, storageRequest)
			rq.updateMinMax("ephemeral-storage", "limit", matrix, storageLimit)
		}
	}
	return matrix, nil
}

// Helper function to update min and max values in the matrix
func (rq *LimitRanges) updateMinMax(resourceName string, key string, data map[string]map[string]map[string]*LimitRangeDef, value *LimitRangeDef) {
	if _, ok := data[resourceName][key]; !ok {
		data[resourceName][key] = make(map[string]*LimitRangeDef)
		data[resourceName][key]["min"] = NewLimitRangeDef(resourceName, "0", false)
		data[resourceName][key]["max"] = NewLimitRangeDef(resourceName, "0", false)
	}
	min := data[resourceName][key]["min"]
	max := data[resourceName][key]["max"]

	if min.GetValInt() <= 0 || compareQuantities(value, min) < 0 {
		rq.display.Debug(
			fmt.Sprintf(
				ddInitDetectMinMaxUpdateMin,
				key,
				resourceName,
				value.GetValStringUnit(),
			),
		)
		data[resourceName][key]["min"] = value
	}
	if max.GetValInt() <= 0 || compareQuantities(value, max) > 0 {
		rq.display.Debug(
			fmt.Sprintf(
				ddInitDetectMinMaxUpdateMax,
				key,
				resourceName,
				value.GetValStringUnit(),
			),
		)
		data[resourceName][key]["max"] = value
	}
}

// Helper function to compare resource quantities
func compareQuantities(q1 *LimitRangeDef, q2 *LimitRangeDef) int {
	q1Val, _ := resource.ParseQuantity(q1.GetValStringUnit())
	q2Val, _ := resource.ParseQuantity(q2.GetValStringUnit())
	return q1Val.Cmp(q2Val)
}

// Helper function to print min and max values
func printMinMax(resourceName string, scopeName string, matrix map[string]map[string]map[string]*LimitRangeDef) {
	fmt.Printf(
		"%s - %s [Min: %s, Max: %s]\n",
		resourceName,
		scopeName,
		matrix[resourceName][scopeName]["min"].GetValStringUnit(),
		matrix[resourceName][scopeName]["max"].GetValStringUnit(),
	)
}

// Helper function to print min and max values
func PrintMinMaxSynthetis(matrix map[string]map[string]map[string]*LimitRangeDef) {
	printMinMax("cpu", "request", matrix)
	printMinMax("cpu", "limit", matrix)
	printMinMax("memory", "request", matrix)
	printMinMax("memory", "limit", matrix)
	printMinMax("ephemeral-storage", "request", matrix)
	printMinMax("ephemeral-storage", "limit", matrix)
}
