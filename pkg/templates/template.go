package templates

import (
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	v1 "k8s.io/api/core/v1"
	yaml "sigs.k8s.io/yaml"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlimits.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName = "template"
)

// Template represents a template class
type Template struct {
	name    string
	desc    string
	enabled bool
	content string
	display *sxUtils.CmdDisplay
}

// NewTemplate creates a new instance of Template
func NewTemplate(name, desc, content string, enabled bool) *Template {
	return &Template{
		name:    name,
		desc:    desc,
		enabled: enabled,
		content: content,
		display: sxUtils.NewCmdDisplay(GroupName),
	}
}

// GetName returns the name property of the template
func (t *Template) GetName() string {
	return t.name
}

// SetName sets the name property of the template
func (t *Template) SetName(name string) {
	t.name = name
}

// GetDesc returns the desc property of the template
func (t *Template) GetDesc() string {
	return t.desc
}

// SetDesc sets the desc property of the template
func (t *Template) SetDesc(desc string) {
	t.desc = desc
}

// GetEnabled returns the enabled property of the template
func (t *Template) GetEnabled() bool {
	return t.enabled
}

// SetEnabled sets the enabled property of the template
func (t *Template) SetEnabled(enabled bool) {
	t.enabled = enabled
}

// GetContent returns the content property of the template
func (t *Template) GetContent() string {
	return t.content
}

// GetContentYaml returns the content property as a string byte
func (t *Template) GetContentYaml() []byte {
	return []byte(t.content)
}

// GetContentYaml returns the content property as a string byte
func (t *Template) GetContentObj() (*v1.LimitRange, error) {
	var limitRange v1.LimitRange
	err := yaml.Unmarshal([]byte(t.GetContent()), &limitRange)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling template YAML: %v", err)
	}
	return &limitRange, nil
}

// SetContent sets the content property of the template
func (t *Template) SetContent(content string) {
	t.content = content
}

// Display show the template
func (t *Template) GetInfo() {
	t.display.Debug("Display the " + t.GetName() + " template")
	fmt.Printf("%-14s: %s\n", "Name", t.GetName())
	fmt.Printf("%-14s: %-7v\n", "Enabled", t.GetEnabled())
	fmt.Printf("%-14s: %s\n", "Description", t.GetDesc())
}

// Generate a new The name of the limitrange from the template
func (t *Template) GenerateLimitRange(name string) (v1.LimitRange, error) {
	t.display.Debug("Generate " + name + " LimitRange (obj) based on the " + t.GetName() + " template")
	var limitRange v1.LimitRange
	err := yaml.Unmarshal([]byte(t.GetContent()), &limitRange)
	if err != nil {
		return limitRange, fmt.Errorf("error unmarshalling template YAML: %v", err)
	}
	limitRange.ObjectMeta.Name = name
	return limitRange, nil
}

// Generatea new The name of the limitrange from the template
func (t *Template) GenerateLimitRangeYaml(name string) (string, error) {
	t.display.Debug("Generate " + name + " LimitRange (yaml) based on the " + t.GetName() + " template")
	limitRange, err := t.GenerateLimitRange(name)
	if err != nil {
		return "", fmt.Errorf("error generating LimitRange Structure: %v", err)
	}
	yamlBytes, err := yaml.Marshal(limitRange)
	if err != nil {
		return "", fmt.Errorf("error marshalling LimitRange: %v", err)
	}
	return fmt.Sprintf("---\n%s", string(yamlBytes)), nil
}
