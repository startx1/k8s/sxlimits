# Using sxlimits

## Sub-command list

- [Generate a limitRange based on a template](generate.md)
- [Create a limitRange based on a template](create.md)
- [Apply a template to an existing limitRange](apply.md)
- [Resize a limitRange with increment](resize.md)
- [Adjust a limitRange with it's status.used](adjust.md)
- [Describe limitRange content from namespaces](describe.md)
- [Export limitRange content from namespaces](export.md)
- [List all availables templates](templates.md)
- [Get detail about a template](template.md)
- [Get the sxlimits version](version.md)
