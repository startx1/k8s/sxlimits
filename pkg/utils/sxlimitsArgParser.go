package utils

import (
	"context"
	"flag"
	"fmt"

	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxKCfg "gitlab.com/startx1/k8s/go-libs/pkg/kubeconfig"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxLimits "gitlab.com/startx1/k8s/sxlimits/pkg/limitrange"
	sxTpl "gitlab.com/startx1/k8s/sxlimits/pkg/templates"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/kube-openapi/pkg/validation/errors"
)

const (
	// GroupNameSXLimits is the group name use in this package
	GroupNameSXLimits                = "utils.sxlimits"
	ddDebugSXQAPPrepare              = "Prepare the sxLimitsArgParser"
	ddDebugSXQAPPrepareAction        = "Found the action %s"
	ddDebugSXQAPPrepareSubAction     = "Found the sub-action %s"
	ddDebugSXQAPPrepareNeedTemplates = "action %s need templates : %t"
	ddDebugSXQAPPrepareNeedK8sClient = "action %s need k8sclient : %t"
	ddDebugSXQAPPrepareHasNS         = "sxLimitsArgParser has NS  : %t (%s)"
	ddDebugSXQAPPrepareHasKConfig    = "sxLimitsArgParser has Kubeconfig  : %t (%s)"
	ddDebugSXQAPExec                 = "Exec the sxLimitsArgParser"
	ddDebugSXQAPStart                = "Start display the sxLimitsArgParser"
	errSXQAPTemplateReq              = "template name is required for 'template' command"
	errSXQAPPrepare                  = "error executing the SXLimits cli parser : %v"
	msgExecExportRecordFile          = "Recorded all limitRange found in %s namespaces into %s"
	msgExecDescribeOutput            = "Describe all limitRange"
	// imported from main
	ddSeqInit            = "Start the main function"
	ddSeqStart           = "Executing %s subcommand"
	ddCreateStart        = "Start creating the %s limit (based on %s template)"
	ddUpdateStart        = "Start updating the %s limit (based on %s template)"
	ddAdjustStart        = "Start adjusting the %s limit with %s (scope : %s)"
	ddResizeStart        = "Start resizing the %s limit with %s (scope : %s)"
	ddOkCreate           = "LimitRange %s is added into namespace %s"
	ddOkApply            = "LimitRange %s is updated into namespace %s"
	ddOkGenerate         = "LimitRange %s is generated"
	ddOkResize           = "LimitRange %s is resized into namespace %s"
	ddOkAdjust           = "LimitRange %s is adjusted into namespace %s"
	errNoSubcmd          = "Expected 'templates', 'template', 'create', 'adjust', 'resize' or 'export' subcommands. See : sxlimits help."
	errTemplateReq       = "Template name is required for 'template' command"
	errLimitRangeNameReq = "LimitRange name is required for '%s' command"
	errCreateGetTpl      = "Error accessing the template : %v"
	errUpdateGetTpl      = "Error accessing the template : %v"
	errResizeReq         = "limitRange name is required for '%s' command"
	errResizeLoad        = "Error loading limitRange %s in namespace %s : %v"
	errResizeResize      = "Error resizing limitRange %s in namespace %s : %v"
	errResizeApply       = "Error applying the resize limitRange %s in namespace %s : %v"
	errAdjustReq         = "LimitRange name is required for 'adjust' command"
	errAdjustLoad        = "Error loading limitRange %s in namespace %s : %v"
	errAdjustAdjust      = "Error adjusting limitRange %s in namespace %s : %v"
	errAdjustApply       = "Error applying the adjust limitRange %s in namespace %s : %v"
	errApplyLoad         = "Error loading limitRange %s in namespace %s : %v"
	errApplyMerge        = "Error resizing limitRange %s in namespace %s : %v"
	errExportDo          = "Error exporting limitRange %s in namespace %s : %v"
	errApplyApply        = "Error applying %s in namespace %s : %v"
	errCreateGenerate    = "Error generating limitRange %s : %v"
	errApply             = "Error applying %s into the namespace %s : %v"
	errCreate            = "Error creating limitRange %s into the namespace %s : %v"
	errUpdateTemplateReq = "Template name is required for 'update' command"
)

// ArgParser is a definition for displaying message for a command line
type sxLimitsArgParser struct {
	Args          *sxUtils.ArgParser
	Templates     interface{}
	K8sclient     interface{}
	HasNamespace  bool
	Namespace     string
	HasKubeconfig bool
	Kubeconfig    string
}

// Initialize a ArgParser object
// ex:
//
//	display := NewArgParser("main",true)
func NewSXLimitsArgParser(sourceArgs []string) *sxLimitsArgParser {
	argParser := &sxLimitsArgParser{
		Args:         sxUtils.NewArgParser(sourceArgs),
		Templates:    nil,
		K8sclient:    nil,
		HasNamespace: false,
		Namespace:    "",
	}
	return argParser
}

// Display the content of the ArgParser
func (argParser *sxLimitsArgParser) Debug() *sxLimitsArgParser {
	display := sxUtils.NewCmdDisplay(GroupNameSXLimits)
	display.Debug(ddDebugSXQAPStart)
	argParser.Args.Debug()
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareHasNS, argParser.HasNamespace, argParser.Namespace))
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareHasKConfig, argParser.HasKubeconfig, argParser.Kubeconfig))
	return argParser
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxLimitsArgParser) Check() error {
	args := argParser.Args
	if len(args.ParsedArgs) <= 1 {
		return nil
	} else {
		if args.IsHelp {
			return nil
		}
		switch args.TopAction {
		case "template":
			if len(args.ParsedArgs) < 3 {
				return errors.New(20, errSXQAPTemplateReq)
			}
		default:
			return nil
		}
	}
	return nil
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxLimitsArgParser) Prepare() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXLimits)
	needTemplates := false
	needK8sClient := false
	display.Debug(ddDebugSXQAPPrepare)
	args := argParser.Args
	errCheck := argParser.Check()
	if errCheck != nil {
		display.ExitError(
			fmt.Sprintf(
				errSXQAPPrepare,
				errCheck,
			),
			30,
		)
	}
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareAction, args.TopAction))
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareSubAction, args.SubAction))
	switch args.TopAction {
	case "templates", "template", "generate":
		needTemplates = true
	case "update", "create", "apply":
		needTemplates = true
		needK8sClient = true
	case "resize", "adjust", "export", "describe":
		needK8sClient = true
	}
	// check global flags namespace and kubeconfig
	if argParser.Args.HasFlag("--namespace") {
		argParser.HasNamespace = true
		argParser.Namespace = argParser.Args.GetFlagNextVal("--namespace")
		nsPos := argParser.Args.GetFlagPos("--namespace")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
	}
	if argParser.Args.HasFlag("--kubeconfig") {
		argParser.HasKubeconfig = true
		argParser.Kubeconfig = argParser.Args.GetFlagNextVal("--kubeconfig")
		nsPos := argParser.Args.GetFlagPos("--kubeconfig")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
	}
	// load templates if needed
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareNeedTemplates, args.TopAction, needTemplates))
	if needTemplates {
		argParser.Templates = sxTpl.NewTemplateStack()
	}
	// load k8sclient if needed
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareNeedK8sClient, args.TopAction, needK8sClient))
	forceInCluster := true
	if needK8sClient {
		kubeconfig := sxKCfg.NewKubeConfig()
		if argParser.HasKubeconfig {
			kubeconfig.LoadFromFile(argParser.Kubeconfig)
			forceInCluster = false
		} else {
			err := kubeconfig.Load(true)
			if err == nil {
				forceInCluster = false
			}
		}
		argParser.K8sclient = sxKCli.NewK8sClient(kubeconfig.GetPath(), false, forceInCluster)
		if !argParser.HasNamespace {
			argParser.Namespace = argParser.K8sclient.(*sxKCli.K8sClient).GetCurrentNamespace()
		}
	}
	return nil
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxLimitsArgParser) Exec() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXLimits)
	display.Debug(ddDebugSXQAPExec)
	args := argParser.Args

	createCmd := flag.NewFlagSet("create", flag.ExitOnError)
	// Check for main subcommands
	if len(args.ParsedArgs) <= 1 {
		sxLimits.DisplayHelpCmd()
	} else {
		switch args.TopAction {
		case "templates":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			argParser.Templates.(*sxTpl.TemplateStack).DisplayTemplates()
		case "template":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			if args.IsHelp {
				sxTpl.DisplayHelpCmdTemplate()
			} else {
				if len(args.ParsedArgs) < 3 {
					display.ExitError(errTemplateReq, 20)
				}
				argParser.Templates.(*sxTpl.TemplateStack).DisplayTemplateInfo(args.SubAction)
			}
		case "update":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			limitName := "mylimits"
			templateName := "default"
			if args.IsHelp {
				sxLimits.DisplayHelpCmdLimitRangeUpdate()
			} else {
				if len(args.ParsedArgs) < 3 {
					display.ExitError(fmt.Sprintf(errLimitRangeNameReq, args.TopAction), 20)
				}
				limitName = args.SubAction
				createCmd.StringVar(&templateName, "template", "default", "Template name (optional)")
				createCmd.Parse(args.ParsedArgs[3:])
				if len(args.ParsedArgs) > 3 {
					templateName = args.ParsedArgs[3]
				}
				display.Debug(fmt.Sprintf(ddUpdateStart, limitName, templateName))
				template, err := argParser.Templates.(*sxTpl.TemplateStack).GetTemplate(templateName)
				if err != nil {
					display.ExitError(fmt.Sprintf(errUpdateGetTpl, err.Error()), 30)
				}
				// get current namespace
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				if !argParser.HasNamespace {
					argParser.Namespace = k8sClient.GetCurrentNamespace()
				}
				rq := sxLimits.NewLR(limitName, argParser.Namespace, k8sClient)
				errLoad := rq.Load()
				if errLoad != nil {
					display.Error(fmt.Sprintf(errApplyLoad, limitName, argParser.Namespace, errLoad))
					return nil
				}
				errMerge := rq.MergeTemplate(*template)
				if errMerge != nil {
					display.Error(fmt.Sprintf(errApplyMerge, limitName, argParser.Namespace, errMerge))
					return nil
				}
				errApply := rq.Apply()
				if errApply != nil {
					display.Error(fmt.Sprintf(errApplyApply, limitName, argParser.Namespace, errApply))
					return nil
				}
				display.Info(fmt.Sprintf(ddOkApply, limitName, argParser.Namespace))
			}
		case "create", "apply", "generate":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			limitName := "mylimits"
			templateName := "default"
			if args.IsHelp {
				switch args.TopAction {
				case "apply":
					sxLimits.DisplayHelpCmdLimitRangeApply()
				case "generate":
					sxLimits.DisplayHelpCmdLimitRangeGenerate()
				default:
					sxLimits.DisplayHelpCmdLimitRangeCreate()
				}
				return nil
			} else {
				if len(args.ParsedArgs) < 3 {
					display.ExitError(fmt.Sprintf(errLimitRangeNameReq, args.TopAction), 20)
				}
				limitName = args.SubAction
				createCmd.StringVar(&templateName, "template", "default", "Template name (optional)")
				createCmd.Parse(args.ParsedArgs[3:])
				if len(args.ParsedArgs) > 3 {
					templateName = args.ParsedArgs[3]
				}
				display.Debug(fmt.Sprintf(ddCreateStart, limitName, templateName))
				template, err := argParser.Templates.(*sxTpl.TemplateStack).GetTemplate(templateName)
				if err != nil {
					display.ExitError(fmt.Sprintf(errCreateGetTpl, err.Error()), 30)
				}
				if args.TopAction == "generate" {
					// generate the new limitrange from the template
					newLimitRange, err := template.GenerateLimitRangeYaml(limitName)
					if err != nil {
						display.ExitError(fmt.Sprintf(errCreateGenerate, limitName, err.Error()), 30)
					}
					// report it's done for debug
					display.Debug(fmt.Sprintf(ddOkGenerate, limitName))
					// Display yaml result and terminate
					fmt.Println(newLimitRange)
					return nil
				} else if args.TopAction == "apply" {
					// get current namespace
					k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
					if !argParser.HasNamespace {
						argParser.Namespace = k8sClient.GetCurrentNamespace()
					}
					rq := sxLimits.NewLR(limitName, argParser.Namespace, k8sClient)
					errLoad := rq.Load()
					if errLoad != nil {
						display.Error(fmt.Sprintf(errApplyLoad, limitName, argParser.Namespace, errLoad))
						return nil
					}
					errMerge := rq.MergeTemplate(*template)
					if errMerge != nil {
						display.Error(fmt.Sprintf(errApplyMerge, limitName, argParser.Namespace, errMerge))
						return nil
					}
					errApply := rq.Apply()
					if errApply != nil {
						display.Error(fmt.Sprintf(errApplyApply, limitName, argParser.Namespace, errApply))
						return nil
					}
					// Inform end user it's done
					display.Info(fmt.Sprintf(ddOkApply, limitName, argParser.Namespace))
				} else {
					// get current namespace
					k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
					if !argParser.HasNamespace {
						argParser.Namespace = k8sClient.GetCurrentNamespace()
					}
					// generate the new limitrange
					newLimitRange, errCrt := template.GenerateLimitRange(limitName)
					if errCrt != nil {
						display.ExitError(fmt.Sprintf(errCreate, limitName, argParser.Namespace, errCrt), 30)
					}
					// create the new limitrange into the kubernetes cluster
					_, errK8s := k8sClient.Clientset.CoreV1().LimitRanges(argParser.Namespace).
						Create(
							context.Background(),
							&newLimitRange,
							v1.CreateOptions{},
						)
					if errK8s != nil {
						display.ExitError(fmt.Sprintf(errCreate, limitName, argParser.Namespace, errK8s), 30)
					}
					// Inform end user it's done
					display.Info(fmt.Sprintf(ddOkCreate, limitName, argParser.Namespace))
				}
			}
		case "resize", "adjust":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			msgErrReq := fmt.Sprintf(errResizeReq, args.TopAction)
			msgErrLoad := errResizeLoad
			msgErrAction := errResizeResize
			msgErrApply := errResizeApply
			msgDisplayAction := ddResizeStart
			msgDDOk := ddOkResize
			scope := "all"
			if args.IsHelp {
				switch args.TopAction {
				case "apply":
					sxLimits.DisplayHelpCmdLimitRangeAdjust()
				default:
					sxLimits.DisplayHelpCmdLimitRangeResize()
				}
				return nil
			} else {
				if args.TopAction == "adjust" {
					msgErrReq = errAdjustReq
					msgErrLoad = errAdjustLoad
					msgErrAction = errAdjustAdjust
					msgErrApply = errAdjustApply
					msgDisplayAction = ddAdjustStart
					msgDDOk = ddOkAdjust
				}
				if argParser.Args.HasFlag("--scope") {
					scope = argParser.Args.GetFlagNextVal("--scope")
					nsPos := argParser.Args.GetFlagPos("--scope")
					argParser.Args.RemoveFlagFromPos(nsPos)
					argParser.Args.RemoveFlagFromPos(nsPos)
				}

				if len(args.ParsedArgs) < 3 {
					display.ExitError(msgErrReq, 20)
				}
				limitName := args.SubAction
				increment := "0"
				if len(args.ParsedArgs) > 3 {
					increment = args.ParsedArgs[3]
				}
				display.Debug(fmt.Sprintf(msgDisplayAction, limitName, increment, scope))
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				rq := sxLimits.NewLR(limitName, argParser.Namespace, k8sClient)
				errLoad := rq.Load()
				if errLoad != nil {
					display.ExitError(fmt.Sprintf(msgErrLoad, limitName, argParser.Namespace, errLoad), 30)
					return nil
				}
				if args.TopAction == "adjust" {
					errAction := rq.Adjust(increment, scope)
					if errAction != nil {
						display.ExitError(fmt.Sprintf(msgErrAction, limitName, argParser.Namespace, errAction), 30)
						return nil
					}
				} else {
					errAction := rq.Resize(increment, scope)
					if errAction != nil {
						display.ExitError(fmt.Sprintf(msgErrAction, limitName, argParser.Namespace, errAction), 30)
						return nil
					}
				}
				errApply := rq.Apply()
				if errApply != nil {
					display.Error(fmt.Sprintf(msgErrApply, limitName, argParser.Namespace, errApply))
					return nil
				}
				// Inform end user it's done
				display.Info(fmt.Sprintf(msgDDOk, rq.Name, rq.Namespace))
			}

		case "export":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			if args.IsHelp {
				sxLimits.DisplayHelpCmdExport()
			} else {
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				hasHeader := true
				separator := ";"
				outputFile := "-"
				if argParser.Args.HasFlag("--no-header") {
					hasHeader = false
					argParser.Args.RemoveFlagFromPos(argParser.Args.GetFlagPos("--no-header"))
				}
				if argParser.Args.HasFlag("--sep") {
					separator = argParser.Args.GetFlagNextVal("--sep")
					nsPos := argParser.Args.GetFlagPos("--sep")
					argParser.Args.RemoveFlagFromPos(nsPos)
					argParser.Args.RemoveFlagFromPos(nsPos)
				}
				if argParser.Args.HasFlag("--output") {
					outputFile = argParser.Args.GetFlagNextVal("--output")
					nsPos := argParser.Args.GetFlagPos("--output")
					argParser.Args.RemoveFlagFromPos(nsPos)
					argParser.Args.RemoveFlagFromPos(nsPos)
				}

				export, errExport := sxLimits.LRExporter(args.SubAction, k8sClient, separator, hasHeader, false)
				if errExport != nil {
					display.Error(errExport.Error())
					return nil
				}
				if outputFile != "-" {
					sxUtils.WriteFile(outputFile, export)
					display.Info(fmt.Sprintf(msgExecExportRecordFile, args.SubAction, outputFile))
				} else {
					fmt.Println(export)
				}
			}

		case "describe":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			if args.IsHelp {
				sxLimits.DisplayHelpCmdDescribe()
			} else {
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				export, errExport := sxLimits.LRDescriber(args.SubAction, k8sClient)
				if errExport != nil {
					display.Error(errExport.Error())
					return nil
				}
				display.Info(msgExecDescribeOutput)
				fmt.Println(export)
			}
		case "version":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			DisplayVersion()
		default:
			display.Warning(fmt.Sprintf(ddSeqStart, args.TopAction))
			sxLimits.DisplayHelpCmd()
		}
	}
	return nil
}
