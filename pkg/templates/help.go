package templates

import (
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

// DisplayHelpCmdTemplate display the help message for the
// subcommand template
func DisplayHelpCmdTemplate() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the template help message")
	helpMessage := `
Template sub-command allow you to get informations about a given
limitRange template provided by sxlimits

Usage: sxlimits template ARG [OPTIONS]...

Arguments:
  NAME   The name of the template

Generic options:
  --debug      Activates debug mode for detailed troubleshooting information.
  --help       Displays this help message and exits.

Examples:
  Get information about the default template:
  sxlimits template default

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}
