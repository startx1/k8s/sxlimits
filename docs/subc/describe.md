# sxlimits describe

describe sub-command allow you to list all limitRange present in
one or many namespaces and display it in a terminal display. 
You can choose bettween several format option that allow you to aggregate
information by namespace, resource or limitRange.

## Usage

```bash
sxlimits describe [NS_PATTERN] [OPTIONS]...
```

## Arguments

| NAME      | Mandatory | Description                                                                                                                |
| --------- | --------- | -------------------------------------------------------------------------------------------------------------------------- |
| NS_PATTERN | No        | A regex to used for multiple namespace selection. (optional, use SXLIMITS_NS env var or current namespace if not provided) |

## Specific options

| Flag                  | Description                                                                                                                                   |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| --kubeconfig FILEPATH | Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided) |


## Environment variables

| Env name    | Description                                                   |
| ----------- | ------------------------------------------------------------- |
| SXLIMITS_NS | Define the namespace to use instead of the current namespace. |


## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Describe limitrange from the current namespace.

```bash
sxlimits describe 
```

```bash
Namespace  LimitRangeName  Type       Resource           Category        Value         
---------- --------------- ---------- ------------------ --------------- ------------- 
default    full            Pod        cpu                Min             0             
default    full            Pod        memory             Min             0             
default    full            Pod        cpu                Max             0             
default    full            Pod        memory             Max             0             
default    full            Container  memory             Min             0             
default    full            Container  cpu                Min             0             
default    full            Container  cpu                Max             0             
default    full            Container  memory             Max             0             
default    full            Container  cpu                Default         0             
default    full            Container  memory             Default         0             
default    full            Container  cpu                DefaultRequest  0             
default    full            Container  memory             DefaultRequest  0             
default    toto1           Container  memory             Default         2147483648    
default    toto1           Container  cpu                Default         16017         
default    toto1           Container  ephemeral-storage  Default         765577920512  
default    toto1           Container  ephemeral-storage  DefaultRequest  25769803776   
default    toto1           Container  memory             DefaultRequest  20971520      
default    toto1           Container  cpu                DefaultRequest  20
```
