# sxlimits pod template

Template designed for a pod limitRange definition

## Usage

```bash
sxlimits create pod-limits pod
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: pod-limits
spec:
  limits:
  - type: Pod
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
```
