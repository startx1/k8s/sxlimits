# Installation

## 0. Requirements

- Having a RedHat like (Fedora, CentOS, RHEL, ...) operating system
- Install kubectl with `sudo yum install kubectl`

## 1. Get a copy of sxlimits

```bash
curl https://gitlab.com/startx1/k8s/sxlimits/-/raw/stable/bin/sxlimits -o sxlimits
cp sxlimits /usr/local/bin/
chmod ugo+x /usr/local/bin/sxlimits
```
