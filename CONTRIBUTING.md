## How to contribute to this project

#### **Did you find a bug?**

* **Do not open up a GitLab issue if the bug is a security vulnerability**, and instead to refer to our [security policy](https://sxlimits.readthedocs.io/security/).

* **Ensure the bug was not already reported** by searching on GitLab under [Issues](https://gitlab.com/startx1/k8s/sxlimits/issues).

* If you're unable to find an open issue addressing the problem, [open a new one](https://gitlab.com/startx1/k8s/sxlimits/issues/new). Be sure to include a **title and clear description**, as much relevant information as possible, and a **code sample** or an **executable test case** demonstrating the expected behavior that is not occurring.

* If possible, use the relevant bug report templates to create the issue. 

* For more detailed information on submitting a bug report and creating an issue, visit our [reporting guidelines](https://sxlimits.readthedocs.io/contribute.html).

#### **Did you write a patch that fixes a bug?**

* Open a new GitLab pull request with the patch.

* Ensure the PR description clearly describes the problem and solution. Include the relevant issue number if applicable.

* Before submitting, please read the [Contributing guide](https://sxlimits.readthedocs.io/contribute.html) to know more about coding conventions and benchmarks.

#### **Did you fix whitespace, format code, or make a purely cosmetic patch?**

Changes that are cosmetic in nature and do not add anything substantial to the stability, functionality, or testability of application will generally be accepted.

#### **Do you intend to add a new feature or change an existing one?**

* Suggest your change in the [sxlimits mailing list](https://sxlimits.readthedocs.io/mailing-list) and start writing code.

* Do not open an issue on GitLab until you have collected positive feedback about the change. GitLab issues are primarily intended for bug reports and fixes.

#### **Do you have questions about the source code?**

* Ask any question about how to use Ruby on Rails in the [sxlimits mailing list](https://sxlimits.readthedocs.io/mailing-list).

#### **Do you want to contribute to the Rails documentation?**

* Please read [Contributing to the SXLimits Documentation](https://sxlimits.readthedocs.io/contribute.html).

Thanks! :heart: :heart: :heart:

Startx Team
