# sxlimits apply

apply sub-command allow you to apply a template into an existing
limitRange. If limitrange doesn't exist, it will return an error 

## Usage

```bash
sxlimits apply NAME [TEMPLATE] [OPTIONS]...
```

## Arguments

| NAME     | Mandatory | Description                                                     |
| -------- | --------- | --------------------------------------------------------------- |
| NAME     | Yes       | The name of the limitrange                                          |
| TEMPLATE | No        | The name of the template (use default template if not provided) |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Create and improve a stack

Create the example-limit limitRange based on the default template and add pod and container limitRange to it

```bash
echo "---create the content of example-limit"
sxlimits create example-limit default
echo "---display the content of example-limit"
kubectl describe limitrange example-limit
echo "---add the container content to it"
sxlimits apply example-limit container
echo "---display the content of example-limit"
kubectl describe limitrange example-limit
echo "---add the pod content to it"
sxlimits apply example-limit pod
echo "---display the content of example-limit"
kubectl describe limitrange example-limit
```
