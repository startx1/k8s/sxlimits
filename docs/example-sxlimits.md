# The example project

In order to test the sxlimits behavior, you can create the `test-sxlimits` 
namespace with an example workload by using the following script. Be sure to be connected to 
a kubernetes cluster before executing it.

```bash
NS=test-sxlimits
kubectl create ns ${NS} && \
kubectl -n ${NS} create -f https://gitlab.com/startx1/k8s/sxlimits/-/raw/devel/docs/example-sxlimits.yaml && \
kubectl -n ${NS} get pod -o wide && \
kubectl -n ${NS} describe limitrange && \
echo "END creating ${NS}"
```

should return the following result 

```bash
namespace/test-sxlimits created
deployment.apps/http-small created
deployment.apps/http-tmp created
deployment.apps/http-big created
limitrange/default created
NAME                         READY   STATUS    RESTARTS   AGE   IP       NODE     NOMINATED NODE   READINESS GATES
http-big-68ffc7c46f-fmvlt    0/1     Pending   0          1s    <none>   <none>   <none>           <none>
http-small-655c6df99-gs6d8   0/1     Pending   0          1s    <none>   <none>   <none>           <none>
http-tmp-77d554fb7-wh5vf     0/1     Pending   0          1s    <none>   <none>   <none>           <none>
Name:       default
Namespace:  test-sxlimits
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Container   cpu                20m   1      20m              20m            -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
END creating test-sxlimits
```

## Content

### http-small

Small sized deployment with minimal resource consumption.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: http-small
spec:
  replicas: 1
  selector:
    matchLabels:
      app: http-small
  template:
    metadata:
      labels:
        app: http-small
    spec:
      containers:
      - name: server
        image: nginx:1.14.2
        resources:
          limits:
            cpu: 75m
            memory: 64Mi
          requests:
            cpu: 15m
            memory: 32Mi
```

### http-tmp

Small sized deployment with ephemeral-storage consumption.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: http-tmp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: http-tmp
  template:
    metadata:
      labels:
        app: http-tmp
    spec:
      containers:
      - name: server
        image: nginx:1.14.2
        resources:
          limits:
            cpu: 75m
            ephemeral-storage: 1Gi
            memory: 64Mi
          requests:
            cpu: 15m
            ephemeral-storage: 256Mi
            memory: 32Mi
```

### http-big

Big sized deployment with relativly heavy consumption.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: http-big
spec:
  replicas: 1
  selector:
    matchLabels:
      app: http-big
  template:
    metadata:
      labels:
        app: http-big
    spec:
      containers:
      - name: server
        image: nginx:1.14.2
        resources:
          limits:
            cpu: 800m
            memory: 1Gi
          requests:
            cpu: 200m
            memory: 50Mi
```

### Default limitRange

LimitRange sized to fit the previous workloads. 

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: default
spec:
  limits:
  - type: Container
    defaultRequest:
      cpu: 20m
      memory: 32Mi
    default:
      cpu: 20m
      ephemeral-storage: 50Mi
      memory: 48Mi
    min:
      cpu: 20m
      memory: 32Mi
    max:
      cpu: "1000m"
      ephemeral-storage: 500Mi
      memory: 2Gi
  - type: Pod
    min:
      cpu: 20m
      memory: 32Mi
    max:
      cpu: "2"
      ephemeral-storage: 2Gi
      memory: 4Gi
```

If we run the `kubctl describe limitrange default` command, limitRange default should be like this :

```bash
Type        Resource           Min   Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------           ---   ---    ---------------  -------------  -----------------------
Container   ephemeral-storage  -     500Mi  50Mi             50Mi           -
Container   memory             32Mi  2Gi    32Mi             48Mi           -
Container   cpu                20m   1      20m              20m            -
Pod         cpu                20m   2      -                -              -
Pod         ephemeral-storage  -     2Gi    -                -              -
Pod         memory             32Mi  4Gi    -                -              -
```
