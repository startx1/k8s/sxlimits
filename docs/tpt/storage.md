# sxlimits storage template

Template designed for a storage limitRange definition

## Usage

```bash
sxlimits create sto-limits storage
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: sto-limits
spec:
  limits:
  - type: Pod
    max:
      ephemeral-storage: 4Gi
  - type: Container
    default:
      ephemeral-storage: 1Gi
    defaultRequest:
      ephemeral-storage: 20Mi
    max:
      ephemeral-storage: 4Gi
```
