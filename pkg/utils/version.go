package utils

import (
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

const (
	versionMajor = "0.2"
	versionMinor = "7"
)

// Function used to display answer to the help command (or flag)
func DisplayVersion() {
	display := sxUtils.NewCmdDisplay(GroupNameSXLimits)
	display.Debug("Display the version message")
	versionMessage := `sxlimits v` + versionMajor + "." + versionMinor
	fmt.Println(versionMessage)
}
