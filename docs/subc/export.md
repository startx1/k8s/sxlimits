# sxlimits export

export sub-command allow you to return all limitRange present in
one or many namespaces and display or record it as CSV content. 
You can choose bettween several format option that allow you to aggregate
information by namespace, resource or limitRange.

## Usage

```bash
sxlimits export [NS_PATTERN] [OPTIONS]...
```

## Arguments

| NAME      | Mandatory | Description                                                                                                                |
| --------- | --------- | -------------------------------------------------------------------------------------------------------------------------- |
| NS_PATTERN | No        | A regex to used for multiple namespace selection. (optional, use SXLIMITS_NS env var or current namespace if not provided) |

## Specific options

| Flag                  | Description                                                                                                                                   |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| --no-header           | Return no header (default return headers)                                                                                                     |
| --output FILE         | Output content into the given file instead of stdout                                                                                          |
| --sep SEPARATOR       | Use this separator instead of the default ';'                                                                                                 |
| --kubeconfig FILEPATH | Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided |

## Environment variables

| Env name    | Description                                                   |
| ----------- | ------------------------------------------------------------- |
| SXLIMITS_NS | Define the namespace to use instead of the current namespace. |


## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples


### Export limitrange from the current namespace.

```bash
sxlimits export
```

```bash
Namespace;LimitRangeName;Type;Resource;Category;Value
default;full;Pod;cpu;Min;0
default;full;Pod;memory;Min;0
default;full;Pod;memory;Max;0
default;full;Pod;cpu;Max;0
default;full;Container;cpu;Min;0
default;full;Container;memory;Min;0
default;full;Container;cpu;Max;0
default;full;Container;memory;Max;0
default;full;Container;cpu;Default;0
default;full;Container;memory;Default;0
default;full;Container;cpu;DefaultRequest;0
default;full;Container;memory;DefaultRequest;0
default;toto1;Container;memory;Default;2147483648
default;toto1;Container;cpu;Default;16017
default;toto1;Container;ephemeral-storage;Default;765577920512
default;toto1;Container;cpu;DefaultRequest;20
default;toto1;Container;ephemeral-storage;DefaultRequest;25769803776
default;toto1;Container;memory;DefaultRequest;20971520
```

### Export synthetis of all limitrange in all openshift-* namespaces

```bash
sxlimits export openshift-*
```

```bash
Namespace;LimitRangeName;Type;Resource;Category;Value
default;full;Pod;cpu;Min;0
default;full;Pod;memory;Min;0
```


### Export synthetis of all resources in all openshift-* namespaces

```bash
sxlimits export openshift-* --output /tmp/myexport.csv
ls -la /tmp/myexport.csv
```

```bash
-rw-rw-r--. 1 cl cl 3833 May  3 10:55 /tmp/myexport.csv
```