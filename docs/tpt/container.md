# sxlimits container template

Template designed for a container limitRange definition

## Usage

```bash
sxlimits create container-limits container
```

## Example output

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: container-limits
spec:
  limits:
  - type: Container
    default:
      cpu: 200m
      ephemeral-storage: 4Gi
      memory: 256Mi
    defaultRequest:
      cpu: 100m
      ephemeral-storage: 4Gi
      memory: 128Mi
    max:
      cpu: "6"
      ephemeral-storage: 4Gi
      memory: 5Gi
    min:
      cpu: 20m
      memory: 32Mi
```
