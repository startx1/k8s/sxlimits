# sxlimits templates

Templates sub-command allow you to get the list of all limitRange
templates available in sxlimits.

## Usage

```bash
sxlimits templates [OPTIONS]...
```

## Arguments

_no arguments for version_

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

Get the list of all templates.

```bash
sxlimits templates
```
